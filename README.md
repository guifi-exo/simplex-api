# simplex-api

[![LoopBack](https://github.com/strongloop/loopback-next/raw/master/docs/site/imgs/branding/Powered-by-LoopBack-Badge-(blue)-@2x.png)](http://loopback.io/)

This project holds the Rest API used in eXO (Expansió de la Xarxa Oberta). In the moment of writing this README the only feature covered is the subscriptions management (registrations/cancels)

## Specification

[simplex.md](./simplex.md)

## Configuration
In order to configure simplex custom options we have to modify `simplex-config.json`. Now we support those parameters:
- staticRadGroup: An string which contains static IP Profile ID.
- disableRadGroup: An string which contains Disabled Profile ID.
- managementRadGroup: An string which contains Management Profile ID.
- radius:
  - realm: Realm domain for your ISP i.g: `exo.cat`
  - daeHost: Dae NAS Host
  - daePort: Dae NAS port
  - daeSecret: Secret for radius packet
  - timeout: For receiving UDP response

## Scripts
Before run any script you must build the solution. Please run `npm run build`.

### Consistency
This script is used to ensure consistency between simplex datasources and RADIUS datasources. There are several functionalities covered:
- syncradius: This ensures consistency between RADIUS and simplex tables
- ipscircuit: Takes IPv4 and IPv6 from circuits and make them consistent in the IP pools
- ipsradius: Takes IPv4 and IPv6 from RADIUS tables and make them consistent in the IP pools
- voidsubs: It deletes all subscriptions without circuits
- ipv6notation: Fixes bad ipv6notation in RADIUS radReply table

Usage: `npm run consistency -- <action> [-f if we want to fix the issues]`

### Operations
This script is used in server side to perform operations in the database. Now supported options are:
- initialsubs: This option creates available subscription ids between min and max. Syntax: `npm run operations -- initialsubs  -o "{"min": <int>, "max": <int> }"`
- addipv4range: This option creates ipv4 ranges in the pool. Syntax: `npm run operations -- addipv4range -o "{"subnet": "<a.b.c.d/i>", "available": <boolean> }"`
- migrateipv4: This option changes ipv4 from circuits and radius tables. Syntax: `npm run operations -- addipv4range -o "{"oldSubnet": "<a.b.c.d/i>", "newSubnet": "<a.b.c.d/i>", "available": <boolean>, "exceptions": {"<a.b.c.d>": true... } }"`
- removeipv4range: This option removes ipv4 subnet from pool. Syntax: `npm run operations -- addipv4range -o "{"subnet": "<a.b.c.d/i>"}"`


## Contribute
This software is developed using the node.js runtime, so you need an environment with it. You also need a MySQL or mariaDB database running in your system. In this repo is explained how to use Docker to

You can start developing with these commands:

`git clone https://gitlab.com/guifi-exo/simplex-api.git`

`cd simplex-api; npm install`

`npm run build`

`npm run start`

You also need a valid database running in your computer.

### Multiple datasources
We have moved our data (simplex entities) to a postgresql database. Now this project uses two different database systems:
- mariaDB: For radius stuff
- postgreSQL: For all simplex entities.

In order to create simplex data structure you should run:

`npm run migrate`

### Using Docker to provide databases
It's a common practice the use of Docker container system to deploy the services needed in the development process. The next tips help you to run the environment using Docker:

- It's necessary to have installed `docker` and `docker-compose`
- You can install Docker in Debian using this guide: https://docs.docker.com/install/linux/docker-ce/debian/
- You can install `docker-compose`:
  - `sudo apt install python3-pip; sudo pip install docker-compose`
- Then simply run: `docker-compose up`

By default, the radius database is populated using `/dump/radius.sql` contents.

This method provides both databases.

## License
This software is licensed under GNU Affero General Public License v3.0.

Copyright 2020 Roger Garcia
