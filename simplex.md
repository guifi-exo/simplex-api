# Introducció

A mesura que l'associació ha anat actualizant la infraestructura de serveis proveïment d'accés a Internet, s'ha incrementat la complexitat dels elements de suport dels processos d'AAA. Els processos adminstratius de gestió de subscripcions a serveis i d'afiliats a la associació s'han sobrecarregat de tal manera, que ja no és possible una gestió manual directe sobre tots els components que hi intervenen.

## Descripció del sistema de proveïment

L'element central del sistema de proveïment és un _Border Network Gateway_ (BNG). Aquest element està format per un servidor L2TP i PPPoE que proporciona connexions PPP als CPE dels subscriptors. Aquests dispositius poden accedir al BNG a través d'una xarxa L3 (Guifi.net) o bé de circuits L2 (bitstreams).

Per tal de centralitzar els processos d'AAA, comptem amb un servidor RADIUS amb registres basats en una base de dades relacional. De moment, només implementem processos d'_Autentication_ i d'_Access_. L'_Accounting_ no està previst que sigui implementar a curt termini.


Les implementacions amb que comptem són a la taula següent:

| Funció             | Implementació  | Versió                       |
|--------------------|----------------|------------------------------|
| L2TP/PPPoE server  | Accel-PPP      | 0.12.0                       |
| RADIUS server      | FreeRADIUS     | 3.0.12+dfsg-5+deb9u1         |
| Database           | MySQL          | 15.1 Distrib 10.1.41-MariaDB |

El següent esquema representa la topologia dels elements esmentats:

```
                    Components d'infraestructura de provisionament del BNG


                                                                      +----------+
              +-----------+               +------------+              |   SQL    |
+-----+       |           |  socket:1701  |            |  socket:SQL  |          +---+
| CPE +-------+    BNG    +<------------->+   RADIUS   |<------------>+ radreply |   |
+-----+       |           |<--------+     |            |              +----------+   +---+
              +-----------+         |     +------------+                  | radcheck |   |
                    ^               |                                     +----------+   |
                    | socket:2001   +------------+ socket:3799                |   (...)  |
                    |                            |                            +----------+           
              +-----*-----+               +------*-----+
              |           |               |            |
              | accel-cmd |               | radclient  |
              |           |               |            |
              +-----------+               +------------+
```

Quan un CPE correctament configurat envia al BNG una sol·licitud de connexió, aquest intercanvia els següents missatges amb el servidor RADIUS:

```
[BNG] => [RADIUS] Access-Request id=1 <User-Name "<access_id>@exo.cat"> <Framed-Protocol PPP> <···>
[BNG] <= [RADIUS] Access-Accept id=1 <Framed-Protocol PPP> <Framed-IP-Address 45.150.184.2> <Delegated-IPv6-Prefix 2a0f:de00:fe00:200::/56> <NAS-Port-Id "<access_id>"> <···>
```
Vegeu més detalls en aquest [document](https://gitlab.com/guifi-exo/projectes/blob/master/access-services-bitstreams.md#definici%C3%B3-de-serveis-finals-i-portadors).

FreeRADIUS consulta en primer lloc la taula _radcheck_ per verificar que el password transferit pel CPE (MS-CHAP/CHAP) és correcte. En el mateix moment, envia al NAS (BNG) els atributs necessaris:

```
attribute = Framed-IP-Address
attribute = Delegated-IPv6-Prefix
attribute = NAS-Port-Id
```
Aquests valors són a les taules _radreply_ segons consultes creuades a d'altres taules (_radgroupreply_, _radusergroup_). Hem de tenir en compte que aquestes taules registren els circuits de provisió no de les persones subscrites.


# Requeriments de SIMPLEX

Es plantega la implementació d'un programri que permeti integrar les actuals funcions de gestió de circuits de provisionament (connexions PPP) amb funciones extra orientades a gestionar els perfils de persones afiliades a l'associació. Es planteja doncs:

* Afegir un registre de dades personals de persones afiliades
* Afegir un registre de serveis (p.ex: Basic Radio, Sarenet Competitive, EXtra IPv4, etc.)
* Les funcions fonamentals que caldrà implementar són:
  - Afegir/eliminar/modificar nova persona afiliada, afegir/eliminar nou servei
  - Afegir/eliminar/modificar circuit de provisionament: credencials, IPv4/IPv6
  - Obtenir llistes d'afiliacions i d'alres dades relacionades amb el servei subscrit
  - Obtenir dades sobre l'estat de les connexios (BNG)
  - Enviar missatges de control de la connexió (DAE server)
  - Autenticar, autoritzar i gestionar els comptes dels administradors dels serveis

La solució ha de permetre una alta integració dels diferents agents, és a dir, les aplicacions que puguin dur a terme totes o un group de les funcions esmentades i els elements existents (BNG, RADIUS, SQL). També ha de permtre una gran modularitat i distribució en xarxa.

# Proposta de disseny

Atés que tots els elements existents basen el seu funcionament amb crides de socket (xarxa) i es requereix una alta integració, proposem el desenvolupament de:rest api authentication ssh key

* Una REST API que implementi les crides accessibles pels agents (aplicacions)
* Un agent basat en text orientat a administradors avançats i que permeti iteracions de desenvolupament assumibles (relativament curtes)
* Un agent basat en web orientat a adminitradors básics i/o avançats

El següent esquema descriu l'arquitectura general de la solució SIMPLEX:

```
                                Arquitectura general de SIMPLEX


              +-----------+               +------------+              +----------+
+-----+       |           |  socket:1701  |            |  socket:SQL  |   SQL    +---+
| CPE +-------+    BNG    +<------------->+   RADIUS   |<------------>+          |   |
+-----+       |           +<--------+     |            |       +----->| radreply |   +---+
              +-----------+         |     +------------+       |      +----------+   |   |
                    ^               |                          |          | radcheck |   |
                    |               |                          |          +----------+   |
                    | socket:2001   +------+ socket:3799       |              |   (...)  |
                    |                      |                   | socket:SQL   +----------+           
                    |                      |                   |          
              +-----*----------------------*-------------------*-------------------------+
              |                                 SAPI                                     |    
              +--------------------------------------------------------------------------+
                                                ^
                                                |
                                                | GET/POST/DELETE URI
                                                |
                                           +----*----+
                                           |         |
                                           | Agent-X |
                                           |         |
                                           +---------+    
```

## Especificacions de les estructures de dades
L'intercanvi de missatges HTTP s'especifica amb un _media type_ tipus JSON. Aquest format és genèric i cal
definir el format del contingut d'aquest tipus de missatges. L'API inclou una sèrie de controladors que gestionen les estructures de dades en format JSON.

### Carrier
La declaració de variables de l'endpoint _Carrier_ és:
```
PATH = /carrier/<CarrierId::Integer>
MEDIA_TYPE = application/json
DATA_STRUCT = {
  "id": <CarrierId::Integer>,
  "name": <CarrierName:String>,
  "data": <OptionalCarrierData::Json>,
  "structure": <SpxDataScheme::TRBEx>
```
En aquest cas, el camp `structure` defineix les dades personalitzades del servei portador que presta el carrier.

### Transport
La declaració de variables de l'endpoint _Transport_ és:
```
PATH = /transport/<TransportId::Integer>
MEDIA_TYPE = application/json
DATA_STRUCT = {
    "id": <TransportId::Integer>,
    "data": <SpxDataScheme::TRBEx>,
    "carrierId": <CarrierId::Integer>
  }
```

### Estructures de dades de serveis portadors
Les següents definicions es poden aplicar a les estructures de dades vinculades als serveis portadors:
```
>> Simplex Data Scheme: TRBE1
{
    "sref": "TRBE1",
    "name": "NEBA",
    "location": "",
    "s_vlan":  {"sid":  "Service VLAN", "vid":  3864, "pcp":  0},
    "c_vlan":  {"sid":  "Customer VLAN", "vid":  -1, "pcp":  0},
    "r_vlan":  {"sid":  "R/S VLAN", "vid":  24, "pcp":  3},
    "endpointDev": "%Echolife EG8010H%",
    "snAuth": "",
    "passAuth": ""
  }

>> Simplex Data Scheme: TRBE2:
{
    "sref": "TRBE2",
    "name": "Orange",
    "location": "",
    "s_vlan":  {"sid":  "Service VLAN", "vid":  3864, "pcp":  0},
    "c_vlan":  {"sid":  "Customer VLAN", "vid":  -1, "pcp":  0},
    "r_vlan":  {"sid":  "R/S VLAN", "vid":  23, "pcp":  0},
    "endpointDev": "%Mikrotik hAPac2%",
    "eoipRemAddress": "%10.254.254.254%",
    "eoipTunId": -1
  }
```

