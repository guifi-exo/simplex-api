import { DefaultTransactionalRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { Circuit, CircuitRelations, Subscription, Transport } from '../models';
import { SimplexDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { SubscriptionRepository, TransportRepository } from '.';

export class CircuitRepository extends DefaultTransactionalRepository<
  Circuit,
  typeof Circuit.prototype.id,
  CircuitRelations
  > {
  public readonly subscription: BelongsToAccessor<
    Subscription,
    typeof Circuit.prototype.id
  >;
  public readonly transport: BelongsToAccessor<
    Transport,
    typeof Circuit.prototype.id
  >;
  constructor(
    @inject('datasources.simplex') dataSource: SimplexDataSource,
    @repository.getter('SubscriptionRepository')
    subscriptionRepositoryGetter: Getter<SubscriptionRepository>,
    @repository.getter('TransportRepository')
    transportRepositoryGetter: Getter<TransportRepository>,
  ) {
    super(Circuit, dataSource);
    this.subscription = this.createBelongsToAccessorFor(
      'subscription',
      subscriptionRepositoryGetter,
    );
    this.transport = this.createBelongsToAccessorFor(
      'transport',
      transportRepositoryGetter,
    );

    this.registerInclusionResolver('subscription', this.subscription.inclusionResolver);
    this.registerInclusionResolver('transport', this.transport.inclusionResolver);
  }
}
