import { DefaultTransactionalRepository } from '@loopback/repository';
import { IPv6Pool, IPv6PoolRelations } from '../models';
import { SimplexDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class IPv6PoolRepository extends DefaultTransactionalRepository<
  IPv6Pool,
  typeof IPv6Pool.prototype.prefix,
  IPv6PoolRelations
  > {
  constructor(
    @inject('datasources.simplex') dataSource: SimplexDataSource,
  ) {
    super(IPv6Pool, dataSource);
  }
}
