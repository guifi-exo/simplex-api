import { DefaultTransactionalRepository } from '@loopback/repository';
import { Radcheck, RadcheckRelations } from '../models';
import { RadiusDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class RadcheckRepository extends DefaultTransactionalRepository<
  Radcheck,
  typeof Radcheck.prototype.id,
  RadcheckRelations
  > {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Radcheck, dataSource);
  }
}
