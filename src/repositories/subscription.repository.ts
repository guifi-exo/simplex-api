import { DefaultTransactionalRepository, HasManyRepositoryFactory, repository } from '@loopback/repository';
import { Subscription, SubscriptionRelations, Circuit } from '../models';
import { SimplexDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { CircuitRepository } from './circuit.repository';

export class SubscriptionRepository extends DefaultTransactionalRepository<
  Subscription,
  typeof Subscription.prototype.id,
  SubscriptionRelations
  > {
  public readonly circuits: HasManyRepositoryFactory<
    Circuit,
    typeof Subscription.prototype.id
  >;
  constructor(
    @inject('datasources.simplex') dataSource: SimplexDataSource,
    @repository.getter('CircuitRepository')
    getCircuitRepository: Getter<CircuitRepository>,
  ) {
    super(Subscription, dataSource);
    this.circuits = this.createHasManyRepositoryFactoryFor(
      'circuits',
      getCircuitRepository,
    );

    this.registerInclusionResolver('circuits', this.circuits.inclusionResolver);
  }
}
