import {DefaultCrudRepository} from '@loopback/repository';
import {Operators, OperatorsRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OperatorsRepository extends DefaultCrudRepository<
  Operators,
  typeof Operators.prototype.id,
  OperatorsRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Operators, dataSource);
  }
}
