import {DefaultCrudRepository} from '@loopback/repository';
import {OperatorsAcl, OperatorsAclRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OperatorsAclRepository extends DefaultCrudRepository<
  OperatorsAcl,
  typeof OperatorsAcl.prototype.id,
  OperatorsAclRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(OperatorsAcl, dataSource);
  }
}
