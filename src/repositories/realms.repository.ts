import {DefaultCrudRepository} from '@loopback/repository';
import {Realms, RealmsRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RealmsRepository extends DefaultCrudRepository<
  Realms,
  typeof Realms.prototype.id,
  RealmsRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Realms, dataSource);
  }
}
