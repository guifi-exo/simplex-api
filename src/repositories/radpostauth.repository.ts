import {DefaultCrudRepository} from '@loopback/repository';
import {Radpostauth, RadpostauthRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RadpostauthRepository extends DefaultCrudRepository<
  Radpostauth,
  typeof Radpostauth.prototype.id,
  RadpostauthRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Radpostauth, dataSource);
  }
}
