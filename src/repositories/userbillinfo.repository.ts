import {DefaultCrudRepository} from '@loopback/repository';
import {Userbillinfo, UserbillinfoRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserbillinfoRepository extends DefaultCrudRepository<
  Userbillinfo,
  typeof Userbillinfo.prototype.id,
  UserbillinfoRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Userbillinfo, dataSource);
  }
}
