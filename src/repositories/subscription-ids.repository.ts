import { DefaultTransactionalRepository } from '@loopback/repository';
import { SubscriptionIds, SubscriptionIdsRelations } from '../models';
import { SimplexDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class SubscriptionIdsRepository extends DefaultTransactionalRepository<
  SubscriptionIds,
  typeof SubscriptionIds.prototype.id,
  SubscriptionIdsRelations
  > {
  constructor(
    @inject('datasources.simplex') dataSource: SimplexDataSource,
  ) {
    super(SubscriptionIds, dataSource);
  }
}
