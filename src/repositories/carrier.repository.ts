import { DefaultTransactionalRepository } from '@loopback/repository';
import { Carrier, CarrierRelations } from '../models';
import { SimplexDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class CarrierRepository extends DefaultTransactionalRepository<
  Carrier,
  typeof Carrier.prototype.id,
  CarrierRelations
  > {
  constructor(
    @inject('datasources.simplex') dataSource: SimplexDataSource,
  ) {
    super(Carrier, dataSource);
  }
}
