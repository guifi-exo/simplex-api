import {DefaultCrudRepository} from '@loopback/repository';
import {Radgroupreply, RadgroupreplyRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RadgroupreplyRepository extends DefaultCrudRepository<
  Radgroupreply,
  typeof Radgroupreply.prototype.id,
  RadgroupreplyRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Radgroupreply, dataSource);
  }
}
