import {DefaultCrudRepository} from '@loopback/repository';
import {Radgroupcheck, RadgroupcheckRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RadgroupcheckRepository extends DefaultCrudRepository<
  Radgroupcheck,
  typeof Radgroupcheck.prototype.id,
  RadgroupcheckRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Radgroupcheck, dataSource);
  }
}
