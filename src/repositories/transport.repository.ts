import { DefaultTransactionalRepository, HasManyRepositoryFactory, repository } from '@loopback/repository';
import { Transport, TransportRelations, Circuit } from '../models';
import { SimplexDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { CircuitRepository } from './circuit.repository';

export class TransportRepository extends DefaultTransactionalRepository<
  Transport,
  typeof Transport.prototype.id,
  TransportRelations
  > {
  public readonly circuits: HasManyRepositoryFactory<
    Circuit,
    typeof Transport.prototype.id
  >;
  constructor(
    @inject('datasources.simplex') dataSource: SimplexDataSource,
    @repository.getter('CircuitRepository')
    getCircuitRepository: Getter<CircuitRepository>,
  ) {
    super(Transport, dataSource);

    this.circuits = this.createHasManyRepositoryFactoryFor(
      'circuits',
      getCircuitRepository,
    );

    this.registerInclusionResolver('circuits', this.circuits.inclusionResolver);
  }
}
