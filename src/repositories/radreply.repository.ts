import { DefaultTransactionalRepository } from '@loopback/repository';
import { Radreply, RadreplyRelations } from '../models';
import { RadiusDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class RadreplyRepository extends DefaultTransactionalRepository<
  Radreply,
  typeof Radreply.prototype.id,
  RadreplyRelations
  > {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Radreply, dataSource);
  }
}
