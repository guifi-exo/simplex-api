import { DefaultTransactionalRepository } from '@loopback/repository';
import { Radusergroup, RadusergroupRelations } from '../models';
import { RadiusDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class RadusergroupRepository extends DefaultTransactionalRepository<
  Radusergroup,
  typeof Radusergroup.prototype.id,
  RadusergroupRelations
  > {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Radusergroup, dataSource);
  }
}
