import { DefaultTransactionalRepository } from '@loopback/repository';
import { IPv4Pool, IPv4PoolRelations } from '../models';
import { SimplexDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class IPv4PoolRepository extends DefaultTransactionalRepository<
  IPv4Pool,
  typeof IPv4Pool.prototype.ip,
  IPv4PoolRelations
  > {
  constructor(
    @inject('datasources.simplex') dataSource: SimplexDataSource,
  ) {
    super(IPv4Pool, dataSource);
  }
}
