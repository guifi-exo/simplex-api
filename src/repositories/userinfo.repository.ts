import { DefaultTransactionalRepository } from '@loopback/repository';
import { Userinfo, UserinfoRelations } from '../models';
import { RadiusDataSource } from '../datasources';
import { inject } from '@loopback/core';

export class UserinfoRepository extends DefaultTransactionalRepository<
  Userinfo,
  typeof Userinfo.prototype.id,
  UserinfoRelations
  > {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(Userinfo, dataSource);
  }
}
