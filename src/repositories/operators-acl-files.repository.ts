import {DefaultCrudRepository} from '@loopback/repository';
import {OperatorsAclFiles, OperatorsAclFilesRelations} from '../models';
import {RadiusDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class OperatorsAclFilesRepository extends DefaultCrudRepository<
  OperatorsAclFiles,
  typeof OperatorsAclFiles.prototype.id,
  OperatorsAclFilesRelations
> {
  constructor(
    @inject('datasources.radius') dataSource: RadiusDataSource,
  ) {
    super(OperatorsAclFiles, dataSource);
  }
}
