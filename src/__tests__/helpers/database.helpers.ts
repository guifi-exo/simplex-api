import {
  SubscriptionRepository, CircuitRepository,
  SubscriptionIdsRepository,
  IPv4PoolRepository,
  IPv6PoolRepository,
  RadcheckRepository,
  RadgroupcheckRepository,
  RadgroupreplyRepository,
  RadpostauthRepository,
  RadreplyRepository,
  RadusergroupRepository,
  UserinfoRepository,
  TransportRepository,
  CarrierRepository
} from '../../repositories';
import { SubscriptionsManagerService } from '../../services';
import { SimplexConfig } from '../../application';
import { testdb } from '../datasources/radius.datasource';
import { testSimplexDb } from '../datasources/simplex.datasource';

import { SubscriptionIds, Circuit, Userinfo, Radcheck, Radusergroup, Radreply, Carrier, Transport } from '../../models';

export async function givenEmptyDatabase() {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;

  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  const subscriptonIdsRepository = new SubscriptionIdsRepository(testSimplexDb);
  const carrierRepository = new CarrierRepository(testSimplexDb);
  const ipv4PoolRepository = new IPv4PoolRepository(testSimplexDb);
  const ipv6PoolRepository = new IPv6PoolRepository(testSimplexDb);
  const radCheckRepository = new RadcheckRepository(testdb);
  const radGroupCheckRepository = new RadgroupcheckRepository(testdb);
  const radGroupReplyRepository = new RadgroupreplyRepository(testdb);
  const radPostAuthRepository = new RadpostauthRepository(testdb);
  const radReplyRepository = new RadreplyRepository(testdb);
  const radUserGroupRepository = new RadusergroupRepository(testdb);
  const userInfoRepository = new UserinfoRepository(testdb);

  await circuitRepository.deleteAll();
  await subscriptionRepository.deleteAll();
  await subscriptonIdsRepository.deleteAll();
  await ipv4PoolRepository.deleteAll();
  await ipv6PoolRepository.deleteAll();
  await radCheckRepository.deleteAll();
  await radGroupCheckRepository.deleteAll();
  await radGroupReplyRepository.deleteAll();
  await radPostAuthRepository.deleteAll();
  await radReplyRepository.deleteAll();
  await radUserGroupRepository.deleteAll();
  await userInfoRepository.deleteAll();
  await transportRepository.deleteAll();
  await carrierRepository.deleteAll();
}

export async function givenSubscription(id: number, firstname: string, lastname: string) {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  const subscriptionIdsRepository = new SubscriptionIdsRepository(testSimplexDb);

  await subscriptionIdsRepository.create(new SubscriptionIds({
    id,
    isAvailable: false
  }));

  await subscriptionRepository.create({
    id,
    firstname,
    lastname
  });
}

export async function givenCircuit(circuit: Partial<Circuit>) {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  return circuitRepository.create(circuit);
}

export async function getSubscriptions() {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  return subscriptionRepository.find();
}

export async function givenSubscriptionIdsPool(pool: any) {
  const subscriptonIdsRepository = new SubscriptionIdsRepository(testSimplexDb);

  await subscriptonIdsRepository.createAll(pool);
}

export async function getSubscriptionIdsPool() {
  const subscriptonIdsRepository = new SubscriptionIdsRepository(testSimplexDb);

  return subscriptonIdsRepository.find();
}

export async function getSubscriptionFromId(id: number) {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  return subscriptionRepository.findById(id);
}

export async function givenIPv4Pool(pool: any) {
  const ipv4PoolRepository = new IPv4PoolRepository(testSimplexDb);

  await ipv4PoolRepository.createAll(pool);
}

export async function givenIPv6Pool(pool: any) {
  const ipv6PoolRepository = new IPv6PoolRepository(testSimplexDb);

  await ipv6PoolRepository.createAll(pool);
}

export async function givenRADIUSUserInfo(userinfo: Userinfo) {
  const userInfoRepository = new UserinfoRepository(testdb);

  await userInfoRepository.create(userinfo);
}

export async function givenRADIUSRadCheck(radcheck: Radcheck) {
  const radCheckRepository = new RadcheckRepository(testdb);

  await radCheckRepository.create(radcheck);
}

export async function givenRADIUSRadUserGroup(usergroup: Radusergroup) {
  const radUserGroupRepository = new RadusergroupRepository(testdb);

  await radUserGroupRepository.create(usergroup);
}

export async function givenRADIUSRadReply(radreply: Radreply) {
  const radReplyRepository = new RadreplyRepository(testdb);

  await radReplyRepository.create(radreply);
}

export async function getRADIUSUserInfo(username: string) {
  const userInfoRepository = new UserinfoRepository(testdb);

  return userInfoRepository.find({ where: { username } });
}

export async function getRADIUSRadCheck(username: string) {
  const radCheckRepository = new RadcheckRepository(testdb);

  return radCheckRepository.find({ where: { username } });
}

export async function getRADIUSRadUserGroup(username: string) {
  const radUserGroupRepository = new RadusergroupRepository(testdb);

  return radUserGroupRepository.find({ where: { username } });
}

export async function getRADIUSRadReply(username: string) {
  const radReplyRepository = new RadreplyRepository(testdb);

  return radReplyRepository.find({ where: { username } });
}

export async function givenCarrier(carrier: Partial<Carrier>) {
  const carrierRepository = new CarrierRepository(testSimplexDb);

  return carrierRepository.create(carrier);
}

export async function getCarrierFromId(id: number) {
  const carrierRepository = new CarrierRepository(testSimplexDb);

  return carrierRepository.findById(id);
}

export async function getCarrierFromName(name: string) {
  const carrierRepository = new CarrierRepository(testSimplexDb);

  return carrierRepository.findOne({ where: { name } });
}

export async function getCarriers() {
  const carrierRepository = new CarrierRepository(testSimplexDb);

  return carrierRepository.find();
}

export async function givenTransport(transport: Partial<Transport>) {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  return transportRepository.create(transport);
}

export async function getTransportFromId(id: number) {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  return transportRepository.findById(id);
}

export async function getTransports() {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);

  return transportRepository.find();
}

export function getSubscriptionsManagerService(config: SimplexConfig) {
  let subscriptionRepository: SubscriptionRepository;
  let circuitRepository: CircuitRepository;
  let transportRepository: TransportRepository;

  const radReplyRepository = new RadreplyRepository(testdb);
  const subscriptionIdsRepository = new SubscriptionIdsRepository(testSimplexDb);
  const userInfoRepository = new UserinfoRepository(testdb);
  const radCheckRepository = new RadcheckRepository(testdb);
  const radUserGroupRepository = new RadusergroupRepository(testdb);
  const ipv4PoolRepository = new IPv4PoolRepository(testSimplexDb);
  const ipv6PoolRepository = new IPv6PoolRepository(testSimplexDb);
  subscriptionRepository = new SubscriptionRepository(testSimplexDb, async () => circuitRepository);
  circuitRepository = new CircuitRepository(testSimplexDb, async () => subscriptionRepository, async () => transportRepository);
  transportRepository = new TransportRepository(testSimplexDb, async () => circuitRepository);
  return new SubscriptionsManagerService(radReplyRepository, subscriptionIdsRepository, userInfoRepository,
    radCheckRepository, radUserGroupRepository,
    subscriptionRepository, circuitRepository,
    ipv4PoolRepository, ipv6PoolRepository,
    transportRepository, config);
}
