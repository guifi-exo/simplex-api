import * as radius from 'radius';
import * as dgram from 'dgram';
import { AddressInfo } from 'net';

export interface RadiusServerHelperConfig {
  secret: string;
  port: number;
  receive: {
    (packet: radius.RadiusPacket): radius.RadiusPacket
  };
};

export function setupRadiusServer(config: RadiusServerHelperConfig) {
  let server = dgram.createSocket("udp4");

  server.on("message", (msg, rinfo) => {
    let code;
    const packet = radius.decode({ packet: msg, secret: config.secret });

    // We send this callback to test
    const responsePacket = config.receive(packet);

    // We send back the response
    const response = radius.encode_response({
      packet: responsePacket,
      code: responsePacket.code,
      secret: config.secret
    });

    server.send(response, 0, response.length, rinfo.port, rinfo.address, (err, bytes) => {
      if (err) {
        console.log('Error sending response to ', rinfo);
      }
    });
  });

  server.on("listening", function () {
    const address = <AddressInfo>server.address();
    console.log("radius server listening " +
      address.address + ":" + address.port);
  });

  server.bind(config.port);

  return server;
}
