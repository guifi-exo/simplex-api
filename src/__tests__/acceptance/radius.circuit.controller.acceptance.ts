import { Client, expect } from '@loopback/testlab';
import { ExoApiApplication } from '../..';
import { setupApplication } from '../helpers/test-helper';
import { givenEmptyDatabase, givenSubscription, givenCircuit, givenIPv4Pool, givenIPv6Pool, getRADIUSUserInfo, getRADIUSRadCheck, getRADIUSRadUserGroup, getRADIUSRadReply, givenRADIUSUserInfo, givenRADIUSRadCheck, givenRADIUSRadUserGroup, givenRADIUSRadReply } from '../helpers/database.helpers';
import { Circuit, Radusergroup, Userinfo, Radcheck, Radreply } from '../../models';
import { SimplexConfig } from '../../application';

// Function to test if radius data has been erased
async function checkRadiusRemovedCircuit(circuit: Circuit) {
  const userinfo = await getRADIUSUserInfo(circuit.username.valueOf());

  // We check empty (unique username)
  expect(userinfo).to.be.empty();

  const radcheck = await getRADIUSRadCheck(circuit.username.valueOf());
  expect(radcheck).to.be.empty();

  const radgroup = await getRADIUSRadUserGroup(circuit.username.valueOf());
  expect(radgroup).to.be.empty();

  if (circuit.hasManagement) {
    // We should test also management user
    const radgroupMan = await getRADIUSRadUserGroup
      (`m${circuit.subscriptionId.toString().padStart(4, '0')}c${circuit.circuitNum}`);
    expect(radgroupMan).to.be.empty();
  }

  // Should have nasId, ipv4 & ipv6 (prefix)
  const radreply = await getRADIUSRadReply(circuit.username.valueOf());
  expect(radreply).to.be.empty();
}

// Function to test if radius data is present
async function checkRadiusCircuit(circuit: Circuit, config: SimplexConfig) {
  const userinfo = await getRADIUSUserInfo(circuit.username.valueOf());

  // We check size = 1 (unique username)
  expect(userinfo).to.have.lengthOf(1);
  expect(userinfo[0]).to.have.property('username').which.is.eql(circuit.username);

  const radcheck = await getRADIUSRadCheck(circuit.username?.valueOf());

  // We check size = 1 (unique username -> password)
  expect(radcheck).to.have.lengthOf(1);
  expect(radcheck[0]).to.have.property('username').which.is.eql(circuit.username);
  expect(radcheck[0]).to.have.property('op').which.is.eql(':=');
  expect(radcheck[0]).to.have.property('attribute').which.is.eql('Cleartext-Password');
  expect(radcheck[0]).to.have.property('value').which.is.eql(circuit.password);

  const radgroup = await getRADIUSRadUserGroup(circuit.username.valueOf());
  const radgroupEntry = {
    username: circuit.username,
    groupname: config.staticRadGroup,
    priority: 1
  };

  expect(radgroup).to.matchAny(radgroupEntry);

  if (circuit.hasManagement) {
    // We should test also management user
    const radgroupManEntry = {
      username: `m${circuit.subscriptionId.toString().padStart(4, '0')}c${circuit.circuitNum}`,
      groupname: config.managementRadGroup,
      priority: 1
    };
    const radgroupMan = await getRADIUSRadUserGroup(radgroupEntry.username.valueOf());
    expect(radgroupMan).to.matchAny(radgroupManEntry);
  }

  // Should have nasId, ipv4 & ipv6 (prefix)
  const radreply = await getRADIUSRadReply(circuit.username.valueOf())
  expect(radreply).to.have.lengthOf(3);
  expect(radreply).to.matchAny({
    username: circuit.username,
    op: ':=',
    attribute: 'Framed-IP-Address',
    value: circuit.ipv4
  });
  expect(radreply).to.matchAny({
    username: circuit.username,
    op: ':=',
    attribute: 'Delegated-IPv6-Prefix',
    value: circuit.ipv6
  });
  expect(radreply).to.matchAny({
    username: circuit.username,
    op: ':=',
    attribute: 'NAS-Port-Id',
    value: circuit.nasId
  });
}

describe('RadiusController (Circuit)', () => {
  let app: ExoApiApplication;
  let client: Client;

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  // We load fixtures & helpers
  beforeEach(givenEmptyDatabase);

  after(async () => {
    await app.stop();
  });

  it('Should get all circuits of one subscriber', async () => {
    const id = 10;
    const elements = 5;

    await givenSubscription(10, 'test', 'circuit');

    const circuits = [];
    for (let i = 0; i < elements; ++i) {
      circuits.push(await givenCircuit({
        circuitNum: i,
        hasManagement: false,
        ipv4: `10.0.0.${i}`,
        ipv6: `fe80::${i}`,
        subscriptionId: id,
        nasId: `i${i}`,
        isDisabled: false,
        username: `test${i}`,
        password: `test${i}`
      }));
    }

    const res = await client.get(`/radius/subscription/${id}`).expect(200);
    // It should have given content

    circuits.forEach((item) => {
      // @ts-ignore
      item.transportId = null;
    });
    expect(circuits).to.match(res.body.circuits);
  });

  it('Should create one circuit using pool (enough ips, no management)', async () => {
    const id = 10;
    const elements = 5;

    await givenSubscription(10, 'test', 'circuit');

    const ipv4Pool = [];
    const ipv6Pool = [];

    for (let i = 0; i < elements; ++i) {
      ipv4Pool.push({
        ip: `10.0.0.${i}`,
        isAvailable: true
      });
      ipv6Pool.push({
        prefix: `fe80::${i}`,
        isAvailable: true
      });
    }

    await givenIPv4Pool(ipv4Pool);
    await givenIPv6Pool(ipv6Pool);

    const res = await client.post(`/radius/subscription/${id}/circuit`)
      .send({
        hasManagement: false
      })
      .expect(200);

    // We should check res match with given ips
    expect(ipv4Pool).to.matchAny((value: any) => expect(value.ip).to.be.eql(res.body.ipv4));
    expect(ipv6Pool).to.matchAny((value: any) => expect(value.prefix).to.be.eql(res.body.ipv6));

    // We should check RADIUS tables
    const config: SimplexConfig = <SimplexConfig>await app.get('config.simplex');
    await checkRadiusCircuit(res.body, config);
  });

  it('Should delete one circuit', async () => {
    await givenSubscription(1, 'test', 'circuit');

    const circuit = await givenCircuit({
      circuitNum: 0,
      hasManagement: false,
      ipv4: `10.0.0.1`,
      ipv6: `fe80::1`,
      subscriptionId: 1,
      nasId: `i0001c0`,
      isDisabled: false,
      username: `i0001c0`,
      password: `i0001c0`
    });

    await givenIPv4Pool([{
      ip: `10.0.0.1`,
      isAvailable: false
    }]);

    await givenIPv6Pool([{
      prefix: `fe80::1`,
      isAvailable: false
    }]);

    await givenRADIUSUserInfo(new Userinfo({
      firstname: 'test',
      lastname: 'circuit'
    }));

    await givenRADIUSRadCheck(new Radcheck({
      username: circuit.username,
      op: ':=',
      attribute: 'Cleartext-Password',
      value: circuit.password
    }));

    const config: SimplexConfig = <SimplexConfig>await app.get('config.simplex');

    await givenRADIUSRadUserGroup(new Radusergroup({
      username: circuit.username,
      groupname: config.staticRadGroup,
      priority: 1
    }));

    await givenRADIUSRadReply(new Radreply({
      username: circuit.username,
      op: ':=',
      attribute: 'Framed-IP-Address',
      value: circuit.ipv4
    }));

    await givenRADIUSRadReply(new Radreply({
      username: circuit.username,
      op: ':=',
      attribute: 'Delegated-IPv6-Prefix',
      value: circuit.ipv6
    }));

    await givenRADIUSRadReply(new Radreply({
      username: circuit.username,
      op: ':=',
      attribute: 'NAS-Port-Id',
      value: circuit.nasId
    }));

    const res = await client.delete(`/radius/subscription/${circuit.subscriptionId}/circuit/${circuit.id}`).expect(200);
    expect(res.body).to.be.eql({ msg: 'ok' });

    await checkRadiusRemovedCircuit(circuit);
  });
});
