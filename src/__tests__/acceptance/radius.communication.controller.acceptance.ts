import { Client, expect } from '@loopback/testlab';
import { ExoApiApplication } from '../..';
import { setupApplication } from '../helpers/test-helper';
import { givenEmptyDatabase, givenCircuit, givenSubscription } from '../helpers/database.helpers';
import * as radius from 'radius';
import { SimplexConfig } from '../../application';
import { setupRadiusServer, RadiusServerHelperConfig } from '../helpers/radius.helper';


describe('RadiusController (Radius)', () => {
  let app: ExoApiApplication;
  let client: Client;

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  // We load fixtures & helpers
  beforeEach(givenEmptyDatabase);

  after(async () => {
    await app.stop();
  });

  it('Should disconnect one circuit', async () => {
    let appConfig: SimplexConfig = <SimplexConfig>await app.get('config.simplex');
    // We should set radius server to localhost (dummy)
    appConfig.radius.daeHost = '127.0.0.1';
    app.bind('config.simplex').to(appConfig);
    await givenSubscription(1, 'test', 'circuit');

    const circuit = await givenCircuit({
      circuitNum: 0,
      hasManagement: false,
      ipv4: `10.0.0.1`,
      ipv6: `fe80::1`,
      subscriptionId: 1,
      nasId: `i0001c0`,
      isDisabled: false,
      username: `i0001c0`,
      password: `i0001c0`
    });

    const radPromise = new Promise((resolve, reject) => {
      // We load radius dummy server
      const radiusConfig: RadiusServerHelperConfig = {
        port: appConfig.radius.daePort,
        secret: appConfig.radius.daeSecret,
        receive: ((packet: radius.RadiusPacket) => {
          expect(packet.code).to.be.exactly('Disconnect-Request');
          expect(packet.attributes).to.match({
            'NAS-IP-Address': appConfig.radius.daeHost,
            'User-Name': `${circuit.username}@${appConfig.radius.realm}`
          });
          packet.code = 'Disconnect-ACK';
          return packet;
        })
      };
      const server = setupRadiusServer(radiusConfig);
      client.patch(`/radius/subscription/${circuit.subscriptionId}/circuit/${circuit.id}/disconnect`).expect(200).then((res) => {
        expect(res.body).to.match({ msg: 'ok' });
        resolve();
      }).catch((err) => {
        reject(err);
      }).finally(() => {
        server.close();
      });
    });

    return radPromise;
  });
});
