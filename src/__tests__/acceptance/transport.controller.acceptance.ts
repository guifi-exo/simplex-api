import { Client, expect } from '@loopback/testlab';
import { ExoApiApplication } from '../..';
import { setupApplication } from '../helpers/test-helper';
import { givenEmptyDatabase, givenCarrier, getCarriers, getCarrierFromId, getCarrierFromName, givenTransport, getTransports, getTransportFromId } from '../helpers/database.helpers';

describe('TransportController', () => {
  let app: ExoApiApplication;
  let client: Client;

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  // We load fixtures & helpers
  beforeEach(givenEmptyDatabase);

  after(async () => {
    await app.stop();
  });

  it('Should get N transports from database', async () => {
    const carrier = await givenCarrier({
      name: 'test',
      data: {},
      structure: {}
    });
    const elements = 5;

    const transports = [];

    for (let i = 0; i < elements; ++i) {
      const item = {
        carrierId: carrier.id,
        data: {}
      };
      transports.push(item);
      await givenTransport(item);
    }

    const res = await client.get('/transport').expect(200);
    // It should have given content

    transports.forEach((value) => {
      expect(res.body).matchAny(value);
    });
  });

  it('Should post N transports to database', async () => {
    const carrier = await givenCarrier({
      name: 'test',
      data: {},
      structure: {}
    });

    const elements = 5;

    const postSubscriptions: any[] = [];
    for (let i = 0; i < elements; ++i) {
      const item = {
        carrierId: carrier.id,
        data: {
          test: i
        }
      };

      const res = await client.post('/transport')
        .send(item)
        .expect(200);
      postSubscriptions.push(res.body);
    }

    // Now we should fetch data from database
    const dbSubscriptions = await getTransports();

    expect(dbSubscriptions).to.match(postSubscriptions);
  });

  it('Should delete one transport from database', async () => {
    // We create one subscription
    const carrier = await givenCarrier({
      name: 'test'
    });

    const transport = await givenTransport({
      carrierId: carrier.id,
      data: {}
    });

    await client.delete(`/transport/${transport.id}`)
      .expect(204);

    // We search given id to the database: it must throw an error
    try {
      const res = await getTransportFromId(transport.id.valueOf());
      expect(res).to.be.undefined;
    } catch (err) { }
  });
});
