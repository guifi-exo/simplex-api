import { Client, expect } from '@loopback/testlab';
import { ExoApiApplication } from '../..';
import { setupApplication } from '../helpers/test-helper';
import { givenEmptyDatabase, givenSubscription, givenCircuit, givenIPv4Pool, givenIPv6Pool, getRADIUSUserInfo, getRADIUSRadCheck, getRADIUSRadUserGroup, getRADIUSRadReply, givenRADIUSUserInfo, givenRADIUSRadCheck, givenRADIUSRadUserGroup, givenRADIUSRadReply, getSubscriptionsManagerService } from '../helpers/database.helpers';
import { Circuit, Radusergroup, Userinfo, Radcheck, Radreply } from '../../models';
import { SimplexConfig } from '../../application';
import { SubscriptionsManagerService } from '../../services';


describe('SubscriptionsManagerService (Subscription)', () => {
  let app: ExoApiApplication;
  let client: Client;
  let sms : SubscriptionsManagerService;
  const id = 10;
  const ipv4 = '10.0.0.1';
  const ipv6 = 'fe80:1::';

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  // We load fixtures & helpers
  beforeEach(async () => {
    // Ensure clean database
    await givenEmptyDatabase();
    const config: SimplexConfig = <SimplexConfig>await app.get('config.simplex');
    // This sets up the IP pools with the IPs marked as available
    await givenIPv4Pool([{
      ip: ipv4,
      isAvailable: true
    }]);
    await givenIPv6Pool([{
      prefix: ipv6,
      isAvailable: true
    }]);
    sms = getSubscriptionsManagerService(config);
  });

  after(async () => {
    await app.stop();
  });

  it('Should reserve manually assigned IPv6s to subscriber circuit', async () => {
    // This helper function creates the subscription
    await givenSubscription(id, 'test', 'circuit');

    const circuitData = { hasManagement: false, ipv6: ipv6 };
    await sms.addSubscriptionCircuit(id, circuitData);
    // After adding circuit, ensure IP is registered and NOT available
    const resv6 = await sms.ipv6PoolRepository.findById(ipv6);
    expect(resv6).to.match({ prefix: ipv6, isAvailable: false });
  });

  it('Should reserve manually assigned IPv4s to subscriber circuit', async () => {
    // This helper function creates the subscription
    await givenSubscription(id, 'test', 'circuit');

    const circuitData = { hasManagement: false, ipv4: ipv4 };
    await sms.addSubscriptionCircuit(id, circuitData);
    // After adding circuit, ensure IP is registered and NOT available
    const resv4 = await sms.ipv4PoolRepository.findById(ipv4);
    expect(resv4).to.match({ ip: ipv4, isAvailable: false });
  });
});
