import { Client, expect } from '@loopback/testlab';
import { ExoApiApplication } from '../..';
import { setupApplication } from '../helpers/test-helper';
import { givenEmptyDatabase, givenSubscription, givenSubscriptionIdsPool, getSubscriptions, getSubscriptionFromId, getSubscriptionIdsPool } from '../helpers/database.helpers';

describe('RadiusController (Subscription)', () => {
  let app: ExoApiApplication;
  let client: Client;

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  // We load fixtures & helpers
  beforeEach(givenEmptyDatabase);

  after(async () => {
    await app.stop();
  });

  it('Should get N subscriptions from database', async () => {
    const elements = 5;

    const subscriptions = [];

    for (let i = 0; i < elements; ++i) {
      const item = {
        id: i + 1,
        firstname: `test-${i}`,
        lastname: `testlast-${i}`
      };
      subscriptions.push(item);
      await givenSubscription(item.id, item.firstname, item.lastname);
    }

    const res = await client.get('/radius/subscription').expect(200);
    // It should have given content

    subscriptions.forEach((value) => {
      expect(res.body).containEql(value);
    });
  });

  it('Should post N subscriptions to database', async () => {
    const elements = 5;

    const subscriptionIds = [];

    for (let i = 0; i < elements; ++i) {
      const item = {
        id: i + 1,
        isAvailable: true
      };
      subscriptionIds.push(item);
    }

    await givenSubscriptionIdsPool(subscriptionIds);

    const postSubscriptions: any[] = [];
    for (let i = 0; i < elements; ++i) {
      const item = {
        firstname: `test-${i}`,
        lastname: `testlast-${i}`
      };

      const res = await client.post('/radius/subscription')
        .send(item)
        .expect(200);
      postSubscriptions.push(res.body);
    }

    // Now we should fetch data from database
    const dbSubscriptions = await getSubscriptions();

    expect(dbSubscriptions).to.match(postSubscriptions);
  });

  it('Should delete one subscription from database', async () => {
    // We create one subscription
    await givenSubscription(10, 'test', 'delete');

    const res = await client.delete('/radius/subscription/10')
      .expect(200);

    expect(res.body).to.be.eql({ msg: 'ok' });

    // We search given id to the database: it must throw an error
    try {
      const res = await getSubscriptionFromId(10)
      expect(res).to.be.undefined;
    } catch (err) { }

    const subscriptionIds = await getSubscriptionIdsPool();

    expect(subscriptionIds[0]).to.match({ id: 10, isAvailable: true });
  });
});
