import { Client, expect } from '@loopback/testlab';
import { ExoApiApplication } from '../..';
import { setupApplication } from '../helpers/test-helper';
import { givenEmptyDatabase, givenCarrier, getCarriers, getCarrierFromId, getCarrierFromName } from '../helpers/database.helpers';

describe('CarrierController', () => {
  let app: ExoApiApplication;
  let client: Client;

  before('setupApplication', async () => {
    ({ app, client } = await setupApplication());
  });

  // We load fixtures & helpers
  beforeEach(givenEmptyDatabase);

  after(async () => {
    await app.stop();
  });

  it('Should get N carriers from database', async () => {
    const elements = 5;

    const carriers = [];

    for (let i = 0; i < elements; ++i) {
      const item = {
        name: `test-${i}`,
      };
      carriers.push(item);
      await givenCarrier(item);
    }

    const res = await client.get('/carrier').expect(200);
    // It should have given content

    carriers.forEach((value) => {
      expect(res.body).matchAny(value);
    });
  });

  it('Should post N carriers to database', async () => {
    const elements = 5;

    const postSubscriptions: any[] = [];
    for (let i = 0; i < elements; ++i) {
      const item = {
        name: `test-${i}`,
      };

      const res = await client.post('/carrier')
        .send(item)
        .expect(200);
      postSubscriptions.push(res.body);
    }

    // Now we should fetch data from database
    const dbSubscriptions = await getCarriers();

    expect(dbSubscriptions).to.match(postSubscriptions);
  });

  it('Should delete one carrier from database', async () => {
    // We create one subscription
    const carrier = await givenCarrier({
      name: 'test'
    });

    await client.delete(`/carrier/${carrier.id}`)
      .expect(204);

    // We search given id to the database: it must throw an error
    try {
      const res = await getCarrierFromId(carrier.id.valueOf());
      expect(res).to.be.undefined;
    } catch (err) { }
  });
});
