import * as fs from 'fs';
import { BootMixin } from '@loopback/boot';
import { ApplicationConfig } from '@loopback/core';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import { RepositoryMixin } from '@loopback/repository';
import { RestApplication } from '@loopback/rest';
import { ServiceMixin } from '@loopback/service-proxy';
import * as path from 'path';
import { MySequence } from './sequence';

export interface SimplexConfig {
  staticRadGroup: string;
  disableRadGroup: string;
  managementRadGroup: string;
  radius: {
    realm: string;
    daeHost: string;
    daePort: number;
    daeSecret: string;
    timeout: number;
  }
  subscriptionsMin: number;
  subscriptionsMax: number;
}

export class ExoApiApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up Openapi headers
    this.api({
      openapi: '3.0.0',
      info: {
        title: 'Simplex API',
        version: '1.0.0',
      },
      paths: {}
    });

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.bind(RestExplorerBindings.CONFIG).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };

    // We attach simplex configuration file
    const simplexConfig: SimplexConfig = JSON.parse(
      fs.readFileSync(path.resolve('simplex-config.json')).toString()
    );

    this.bind('config.simplex').to(simplexConfig);
  }
}
