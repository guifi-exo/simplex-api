import { inject } from '@loopback/core';
import { juggler } from '@loopback/repository';
import * as config from './radius.datasource.config.json';

export class RadiusDataSource extends juggler.DataSource {
  static dataSourceName = 'radius';

  constructor(
    @inject('datasources.config.radius', { optional: true })
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
