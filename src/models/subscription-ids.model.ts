import { Entity, model, property } from '@loopback/repository';

@model({ settings: { idInjection: false, postgresql: { table: 'subscriptionids' } } })
export class SubscriptionIds extends Entity {
  @property({
    type: Number,
    generated: false,
    require: true,
    precision: 10,
    scale: 0,
    id: 1
  })
  id: Number;

  @property({
    type: Boolean,
    required: false,
    length: 200,
    postgresql: { "columnName": "isAvailable", "nullable": "N" },
  })
  isAvailable: boolean;



  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<SubscriptionIds>) {
    super(data);
  }
}

export interface SubscriptionIdsRelations {
  // describe navigational properties here
}

export type SubscriptionIdsWithRelations = SubscriptionIds & SubscriptionIdsRelations;
