import { Entity, model, property } from '@loopback/repository';

@model({
  settings: { idInjection: false, mysql: { schema: 'radius', table: 'radusergroup' } }
})
export class Radusergroup extends Entity {
  @property({
    type: String,
    required: true,
    length: 64,
    id: true,
    mysql: { "columnName": "username", "dataType": "varchar", "dataLength": 64, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  username: String;

  @property({
    type: String,
    required: true,
    length: 64,
    mysql: { "columnName": "groupname", "dataType": "varchar", "dataLength": 64, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  groupname: String;

  @property({
    type: Number,
    required: true,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "priority", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  priority: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Radusergroup>) {
    super(data);
  }
}

export interface RadusergroupRelations {
  // describe navigational properties here
}

export type RadusergroupWithRelations = Radusergroup & RadusergroupRelations;
