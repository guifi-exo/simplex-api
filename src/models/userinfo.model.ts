import { Entity, model, property } from '@loopback/repository';

@model({ settings: { idInjection: false, mysql: { schema: 'radius', table: 'userinfo' } } })
export class Userinfo extends Entity {
  @property({
    type: Number,
    generated: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: { "columnName": "id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  id: Number;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "username", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  username?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "firstname", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  firstname?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "lastname", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  lastname?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "email", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  email?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "department", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  department?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "company", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  company?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "workphone", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  workphone?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "homephone", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  homephone?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "mobilephone", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  mobilephone?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "address", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  address?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "city", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  city?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "state", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  state?: String;

  @property({
    type: String,
    required: false,
    length: 100,
    mysql: { "columnName": "country", "dataType": "varchar", "dataLength": 100, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  country?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "zip", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  zip?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "notes", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  notes?: String;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "changeuserinfo", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  changeuserinfo?: String;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "portalloginpassword", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  portalloginpassword?: String;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "enableportallogin", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  enableportallogin?: Number;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "creationdate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationdate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "creationby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationby?: String;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "updatedate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updatedate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "updateby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updateby?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Userinfo>) {
    super(data);
  }
}

export interface UserinfoRelations {
  // describe navigational properties here
}

export type UserinfoWithRelations = Userinfo & UserinfoRelations;
