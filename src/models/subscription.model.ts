import { Entity, model, property, hasMany } from '@loopback/repository';
import { Circuit } from './circuit.model';

@model({
  settings: { idInjection: false, postgresql: { table: 'subscription' } }
})
export class Subscription extends Entity {

  @property({
    type: Number,
    generated: false,
    require: true,
    precision: 10,
    scale: 0,
    id: 1,
  })
  id: Number;

  @property({
    type: String,
    required: true,
    length: 128,
    postgresql: { "columnName": "firstname", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  firstname: String;

  @property({
    type: String,
    required: false,
    length: 128,
    postgresql: { "columnName": "lastname", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  lastname?: String;

  @hasMany(() => Circuit)
  circuits: Circuit[];



  constructor(data?: Partial<Subscription>) {
    super(data);
  }
}

export interface SubscriptionRelations {
  // describe navigational properties here
}

export type SubscriptionWithRelations = Subscription & SubscriptionRelations;
