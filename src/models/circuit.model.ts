import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Subscription, SubscriptionWithRelations } from './subscription.model';
import { Transport } from './transport.model';

@model({
  settings: {
    idInjection: false, postgresql: { table: 'circuit' },
    foreignKeys: {
      fk_circuit_subscriptionId: {
        name: 'fk_circuit_subscriptionId',
        entity: 'Subscription',
        entityKey: 'id',
        foreignKey: 'subscriptionid',
      },
      fk_circuit_transportId: {
        name: 'fk_circuit_transportId',
        entity: 'Transport',
        entityKey: 'id',
        foreignKey: 'transportid',
      },
    },
  }
})
export class Circuit extends Entity {

  @property({
    type: Number,
    generated: true,
    require: true,
    id: 1,
  })
  id: Number;

  @property({
    type: Number,
    generated: false,
    require: true,
  })
  circuitNum: Number;

  @property({
    type: String,
    required: false,
    length: 128,
    postgresql: { "columnName": "username", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  username: String;

  @property({
    type: String,
    required: false,
    length: 128,
    postgresql: { "columnName": "password", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  password: String;

  @property({
    type: String,
    required: false,
    length: 15,
    postgresql: { "columnName": "ipv4", "dataType": "varchar", "dataLength": 15, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  ipv4: String;

  @property({
    type: String,
    required: false,
    length: 15,
    postgresql: { "columnName": "ipv6", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  ipv6: String;

  @property({
    type: String,
    required: false,
    length: 128,
    postgresql: { "columnName": "nasId", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  nasId: String;

  @property({
    type: Boolean,
    required: true,
    postgresql: { "columnName": "hasManagement", "nullable": "N" },
    default: false
  })
  hasManagement: boolean;

  @property({
    type: Boolean,
    required: true,
    postgresql: { "columnName": "isDisabled", "nullable": "N" },
    default: false
  })
  isDisabled: boolean;

  @belongsTo(() => Subscription)
  subscriptionId: number;

  @belongsTo(() => Transport)
  transportId?: number;


  constructor(data?: Partial<Circuit>) {
    super(data);
  }
}

export interface CircuitRelations {
  // describe navigational properties here
  subscription: SubscriptionWithRelations;
}

export type CircuitWithRelations = Circuit & CircuitRelations;
