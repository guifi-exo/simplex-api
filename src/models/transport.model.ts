import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { Carrier } from './carrier.model';
import { Circuit } from './circuit.model';

@model({
  settings: {
    idInjection: false, postgresql: { table: 'transport' },
    foreignKeys: {
      fk_transport_carrierId: {
        name: 'fk_transport_carrierId',
        entity: 'Carrier',
        entityKey: 'id',
        foreignKey: 'carrierid',
      },
    },
  }
})
export class Transport extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'object',
    required: false,
    postgresql: { "columnName": "data", "dataType": "json", "nullable": "Y" },
  })
  data?: any;

  @belongsTo(() => Carrier)
  carrierId?: number;

  @hasMany(() => Circuit)
  circuits: Circuit[];

  constructor(data?: Partial<Transport>) {
    super(data);
  }
}

export interface TransportRelations {
  // describe navigational properties here
}

export type TransportWithRelations = Transport & TransportRelations;
