import { Entity, model, property } from '@loopback/repository';

@model()
export class IPv4Pool extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  ip: string;

  @property({
    type: Boolean,
    required: false,
    length: 1,
    postgresql: { "columnName": "isAvailable", "nullable": "N" },
  })
  isAvailable: boolean;


  constructor(data?: Partial<IPv4Pool>) {
    super(data);
  }
}

export interface IPv4PoolRelations {
  // describe navigational properties here
}

export type IPv4PoolWithRelations = IPv4Pool & IPv4PoolRelations;
