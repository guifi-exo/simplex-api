import { Entity, model, property } from '@loopback/repository';

@model({ settings: { idInjection: false, mysql: { schema: 'radius', table: 'realms' } } })
export class Realms extends Entity {
  @property({
    type: Number,
    generated: true,
    precision: 19,
    scale: 0,
    id: 1,
    mysql: { "columnName": "id", "dataType": "bigint", "dataLength": null, "dataPrecision": 19, "dataScale": 0, "nullable": "N" },
  })
  id: Number;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "realmname", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  realmname?: String;

  @property({
    type: String,
    required: false,
    length: 32,
    mysql: { "columnName": "type", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  type?: String;

  @property({
    type: String,
    required: false,
    length: 256,
    mysql: { "columnName": "authhost", "dataType": "varchar", "dataLength": 256, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  authhost?: String;

  @property({
    type: String,
    required: false,
    length: 256,
    mysql: { "columnName": "accthost", "dataType": "varchar", "dataLength": 256, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  accthost?: String;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "secret", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  secret?: String;

  @property({
    type: String,
    required: false,
    length: 64,
    mysql: { "columnName": "ldflag", "dataType": "varchar", "dataLength": 64, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  ldflag?: String;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "nostrip", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  nostrip?: Number;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "hints", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  hints?: Number;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "notrealm", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  notrealm?: Number;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "creationdate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationdate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "creationby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationby?: String;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "updatedate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updatedate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "updateby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updateby?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Realms>) {
    super(data);
  }
}

export interface RealmsRelations {
  // describe navigational properties here
}

export type RealmsWithRelations = Realms & RealmsRelations;
