import { Entity, model, property } from '@loopback/repository';

@model({
  settings: { idInjection: false, mysql: { schema: 'radius', table: 'userbillinfo' } }
})
export class Userbillinfo extends Entity {
  @property({
    type: Number,
    generated: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: { "columnName": "id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  id: Number;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "username", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  username?: String;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "planName", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  planname?: String;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "hotspot_id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  hotspotId?: Number;

  @property({
    type: String,
    required: false,
    length: 32,
    mysql: { "columnName": "hotspotlocation", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  hotspotlocation?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "contactperson", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  contactperson?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "company", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  company?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "email", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  email?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "phone", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  phone?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "address", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  address?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "city", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  city?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "state", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  state?: String;

  @property({
    type: String,
    required: false,
    length: 100,
    mysql: { "columnName": "country", "dataType": "varchar", "dataLength": 100, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  country?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "zip", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  zip?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "paymentmethod", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  paymentmethod?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "cash", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  cash?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "creditcardname", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creditcardname?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "creditcardnumber", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creditcardnumber?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "creditcardverification", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creditcardverification?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "creditcardtype", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creditcardtype?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "creditcardexp", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creditcardexp?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "notes", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  notes?: String;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "changeuserbillinfo", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  changeuserbillinfo?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "lead", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  lead?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "coupon", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  coupon?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "ordertaker", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  ordertaker?: String;

  @property({
    type: String,
    required: false,
    length: 200,
    mysql: { "columnName": "billstatus", "dataType": "varchar", "dataLength": 200, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  billstatus?: String;

  @property({
    type: Date,
    required: true,
    mysql: { "columnName": "lastbill", "dataType": "date", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  lastbill: Date;

  @property({
    type: Date,
    required: true,
    mysql: { "columnName": "nextbill", "dataType": "date", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  nextbill: Date;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "nextinvoicedue", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  nextinvoicedue?: Number;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "billdue", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  billdue?: Number;

  @property({
    type: String,
    required: false,
    length: 8,
    mysql: { "columnName": "postalinvoice", "dataType": "varchar", "dataLength": 8, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  postalinvoice?: String;

  @property({
    type: String,
    required: false,
    length: 8,
    mysql: { "columnName": "faxinvoice", "dataType": "varchar", "dataLength": 8, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  faxinvoice?: String;

  @property({
    type: String,
    required: false,
    length: 8,
    mysql: { "columnName": "emailinvoice", "dataType": "varchar", "dataLength": 8, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  emailinvoice?: String;

  @property({
    type: Number,
    required: false,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "batch_id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "Y" },
  })
  batchId?: Number;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "creationdate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationdate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "creationby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationby?: String;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "updatedate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updatedate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "updateby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updateby?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Userbillinfo>) {
    super(data);
  }
}

export interface UserbillinfoRelations {
  // describe navigational properties here
}

export type UserbillinfoWithRelations = Userbillinfo & UserbillinfoRelations;
