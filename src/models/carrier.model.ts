import { Entity, model, property } from '@loopback/repository';

@model()
export class Carrier extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'object',
    postgresql: { "columnName": "data", "dataType": "json", "nullable": "Y" },
  })
  data?: object;

  @property({
    type: 'object',
    postgresql: { "columnName": "structure", "dataType": "json", "nullable": "Y" },
  })
  structure?: object;


  constructor(data?: Partial<Carrier>) {
    super(data);
  }
}

export interface CarrierRelations {
  // describe navigational properties here
}

export type CarrierWithRelations = Carrier & CarrierRelations;
