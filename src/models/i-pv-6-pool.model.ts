import {Entity, model, property} from '@loopback/repository';

@model()
export class IPv6Pool extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  prefix: string;

  @property({
    type: 'boolean',
    required: true,
  })
  isAvailable: boolean;


  constructor(data?: Partial<IPv6Pool>) {
    super(data);
  }
}

export interface IPv6PoolRelations {
  // describe navigational properties here
}

export type IPv6PoolWithRelations = IPv6Pool & IPv6PoolRelations;
