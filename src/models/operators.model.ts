import { Entity, model, property } from '@loopback/repository';

@model({ settings: { idInjection: false, mysql: { schema: 'radius', table: 'operators' } } })
export class Operators extends Entity {
  @property({
    type: Number,
    generated: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: { "columnName": "id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  id: Number;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "username", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  username: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "password", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  password: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "firstname", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  firstname: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "lastname", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  lastname: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "title", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  title: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "department", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  department: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "company", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  company: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "phone1", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  phone1: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "phone2", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  phone2: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "email1", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  email1: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "email2", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  email2: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "messenger1", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  messenger1: String;

  @property({
    type: String,
    required: true,
    length: 32,
    mysql: { "columnName": "messenger2", "dataType": "varchar", "dataLength": 32, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  messenger2: String;

  @property({
    type: String,
    required: true,
    length: 128,
    mysql: { "columnName": "notes", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  notes: String;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "lastlogin", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  lastlogin?: Date;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "creationdate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationdate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "creationby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  creationby?: String;

  @property({
    type: Date,
    required: false,
    mysql: { "columnName": "updatedate", "dataType": "datetime", "dataLength": null, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updatedate?: Date;

  @property({
    type: String,
    required: false,
    length: 128,
    mysql: { "columnName": "updateby", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "Y" },
  })
  updateby?: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Operators>) {
    super(data);
  }
}

export interface OperatorsRelations {
  // describe navigational properties here
}

export type OperatorsWithRelations = Operators & OperatorsRelations;
