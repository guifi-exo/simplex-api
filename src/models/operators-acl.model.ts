import { Entity, model, property } from '@loopback/repository';

@model({
  settings: { idInjection: false, mysql: { schema: 'radius', table: 'operators_acl' } }
})
export class OperatorsAcl extends Entity {
  @property({
    type: Number,
    generated: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: { "columnName": "id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  id: Number;

  @property({
    type: Number,
    required: true,
    precision: 10,
    scale: 0,
    mysql: { "columnName": "operator_id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  operatorId: Number;

  @property({
    type: String,
    required: true,
    length: 128,
    mysql: { "columnName": "file", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  file: String;

  @property({
    type: Number,
    required: true,
    precision: 3,
    scale: 0,
    mysql: { "columnName": "access", "dataType": "tinyint", "dataLength": null, "dataPrecision": 3, "dataScale": 0, "nullable": "N" },
  })
  access: Number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<OperatorsAcl>) {
    super(data);
  }
}

export interface OperatorsAclRelations {
  // describe navigational properties here
}

export type OperatorsAclWithRelations = OperatorsAcl & OperatorsAclRelations;
