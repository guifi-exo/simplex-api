import { Entity, model, property } from '@loopback/repository';

@model({ settings: { idInjection: false, mysql: { schema: 'radius', table: 'radreply' } } })
export class Radreply extends Entity {
  @property({
    type: Number,
    precision: 10,
    scale: 0,
    id: 1,
    generated: true,
    mysql: { "columnName": "id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  id: Number;

  @property({
    type: String,
    required: true,
    length: 64,
    mysql: { "columnName": "username", "dataType": "varchar", "dataLength": 64, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  username: String;

  @property({
    type: String,
    required: true,
    length: 64,
    mysql: { "columnName": "attribute", "dataType": "varchar", "dataLength": 64, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  attribute: String;

  @property({
    type: String,
    required: true,
    length: 2,
    mysql: { "columnName": "op", "dataType": "char", "dataLength": 2, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  op: String;

  @property({
    type: String,
    required: true,
    length: 253,
    mysql: { "columnName": "value", "dataType": "varchar", "dataLength": 253, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  value: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Radreply>) {
    super(data);
  }
}

export interface RadreplyRelations {
  // describe navigational properties here
}

export type RadreplyWithRelations = Radreply & RadreplyRelations;
