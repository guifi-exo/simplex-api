import { Entity, model, property } from '@loopback/repository';

@model({
  settings: { idInjection: false, mysql: { schema: 'radius', table: 'operators_acl_files' } }
})
export class OperatorsAclFiles extends Entity {
  @property({
    type: Number,
    generated: true,
    precision: 10,
    scale: 0,
    id: 1,
    mysql: { "columnName": "id", "dataType": "int", "dataLength": null, "dataPrecision": 10, "dataScale": 0, "nullable": "N" },
  })
  id: Number;

  @property({
    type: String,
    required: true,
    length: 128,
    mysql: { "columnName": "file", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  file: String;

  @property({
    type: String,
    required: true,
    length: 128,
    mysql: { "columnName": "category", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  category: String;

  @property({
    type: String,
    required: true,
    length: 128,
    mysql: { "columnName": "section", "dataType": "varchar", "dataLength": 128, "dataPrecision": null, "dataScale": null, "nullable": "N" },
  })
  section: String;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<OperatorsAclFiles>) {
    super(data);
  }
}

export interface OperatorsAclFilesRelations {
  // describe navigational properties here
}

export type OperatorsAclFilesWithRelations = OperatorsAclFiles & OperatorsAclFilesRelations;
