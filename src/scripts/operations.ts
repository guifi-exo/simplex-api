/**
 * Script used to check consistency between circuits, RADIUS tables and IP pools
 */
import 'minimist';
import * as path from 'path';
import * as fs from 'fs';
import IPCIDR from 'ip-cidr';
import { Address6 } from 'ip-address';
import {
  SubscriptionRepository, CircuitRepository,
  SubscriptionIdsRepository,
  IPv4PoolRepository,
  IPv6PoolRepository,
  RadcheckRepository,
  RadgroupcheckRepository,
  RadgroupreplyRepository,
  RadpostauthRepository,
  RadreplyRepository,
  RadusergroupRepository,
  UserinfoRepository,
  TransportRepository
} from '../repositories';
import { RadiusDataSource } from "../datasources/radius.datasource";
import { SimplexDataSource } from "../datasources/simplex.datasource";

const db: RadiusDataSource = new RadiusDataSource();
const simplexDb: SimplexDataSource = new SimplexDataSource();

const subscriptonIdsRepository = new SubscriptionIdsRepository(simplexDb);
const ipv4PoolRepository = new IPv4PoolRepository(simplexDb);
const ipv6PoolRepository = new IPv6PoolRepository(simplexDb);
const radCheckRepository = new RadcheckRepository(db);
const radGroupCheckRepository = new RadgroupcheckRepository(db);
const radGroupReplyRepository = new RadgroupreplyRepository(db);
const radPostAuthRepository = new RadpostauthRepository(db);
const radReplyRepository = new RadreplyRepository(db);
const radUserGroupRepository = new RadusergroupRepository(db);
const userInfoRepository = new UserinfoRepository(db);

let subscriptionRepository: SubscriptionRepository;
let circuitRepository: CircuitRepository;
let transportRepository: TransportRepository;
subscriptionRepository = new SubscriptionRepository(simplexDb, async () => circuitRepository);
circuitRepository = new CircuitRepository(simplexDb, async () => subscriptionRepository,
  async () => transportRepository);
transportRepository = new TransportRepository(simplexDb, async () => circuitRepository);

// We attach simplex configuration file
const simplexConfig: SimplexConfig = JSON.parse(
  fs.readFileSync(path.resolve('simplex-config.json')).toString()
);

import {
  SubscriptionIds,
  Circuit,
  Userinfo,
  Radcheck,
  Radusergroup,
  Radreply,
  IPv4Pool,
  IPv6Pool,
  Subscription,
} from '../models';
import minimist from 'minimist';
import { SimplexConfig } from '../application';
import { IsolationLevel } from '@loopback/repository';
import { BigInteger } from 'jsbn';

interface InitialSubsConf {
  min: number;
  max: number;
}

interface AddIPv4Conf {
  subnet: string;
  available: boolean;
}

interface AddIPv6Conf {
  subnet: string;
  available: boolean;
  toMask: number;
}

interface MigrateIPv4Conf {
  oldSubnet: string;
  newSubnet: string;
  exceptions: {
    [key: string]: string;
  };
}

/**
 * This function deletes all the subscriptionsIds and creates provided subscription between min/max in the system
 * @param conf
 */
async function createInitialSubs(conf: InitialSubsConf) {
  console.log(`Creating initial subscriptions from ${conf.min} to ${conf.max}...`)
  const subs = [];
  await subscriptonIdsRepository.deleteAll();
  for (let i = conf.min; i <= conf.max; ++i) {
    subs.push(new SubscriptionIds({
      id: i,
      isAvailable: true
    }));
  }
  return subscriptonIdsRepository.createAll(subs);
}

/**
 * This function migrates ipv4
 * @param conf
 */
async function migrateIpv4Circuits(conf: MigrateIPv4Conf) {
  console.log(`Migrating IPv4 pool old subnet ${conf.oldSubnet} new subnet ${conf.newSubnet}...`);

  // Prepare transactions
  const txCircuit = await circuitRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
  const txRadReply = await radReplyRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
  const txIPv4Pool = await ipv4PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

  try {
    const oldRange = new IPCIDR(conf.oldSubnet);

    if (!oldRange.isValid()) {
      throw new Error('CIDR is invalid');
    }

    // @ts-ignore
    const addrsOld = <any>oldRange.toArray({ type: "addressObject" });
    const oldipv4: Array<string> = [];


    for (const x in addrsOld) {
      // Ignore exception ips
      const item = addrsOld[x];
      if (!conf.exceptions[item.addressMinusSuffix])
        oldipv4.push(item.addressMinusSuffix);
    }

    // Get used old range ips
    const oldUsed = await circuitRepository.find({ where: { ipv4: { inq: oldipv4 } } }, { transaction: txCircuit });

    const newRange = new IPCIDR(conf.newSubnet);

    if (!newRange.isValid()) {
      throw new Error('CIDR is invalid');
    }

    // @ts-ignore
    const addrsNew = <any>newRange.toArray({ type: "addressObject" });
    const newipv4: Array<string> = []
    for (const x in addrsNew) {
      // Ignore exception ips
      const item = addrsNew[x];
      newipv4.push(item.addressMinusSuffix);
    }

    const newUnused = await ipv4PoolRepository.find({ where: { ip: { inq: newipv4 }, isAvailable: true } }, { transaction: txCircuit });

    // Test length available >= length oldUsed
    if (newUnused.length < oldUsed.length) {
      throw new Error('No enough ipv4 available to allocate');
    }


    for (const x in oldUsed) {
      const item = oldUsed[x];
      console.log(`New ip ${newUnused[x].ip} for ${item.username}`);

      // Change ip in RadReply
      const reply = await radReplyRepository.findOne({ where: { username: item.username, value: item.ipv4 } }, { transaction: txRadReply });
      if (!reply) {
        // Error
        throw new Error('Inconsistency in RADIUS reply table');
      }

      // Change ip in circuit
      item.ipv4 = newUnused[x].ip;
      await circuitRepository.update(item, { transaction: txCircuit });

      // Set old as available
      const ipPool = await ipv4PoolRepository.findOne({ where: { ip: item.ipv4.valueOf() } }, { transaction: txIPv4Pool });

      if (!ipPool) {
        throw new Error('Inconsistency in IP Pool table');
      }
      ipPool.isAvailable = true;

      await ipv4PoolRepository.update(ipPool, { transaction: txIPv4Pool });

      // Set new as used
      newUnused[x].isAvailable = false;

      await ipv4PoolRepository.update(newUnused[x], { transaction: txIPv4Pool });

      reply.value = item.ipv4;

      await radReplyRepository.update(reply, { transaction: txRadReply });

    }
    await txCircuit.commit();
    await txIPv4Pool.commit();
    await txRadReply.commit();
  } catch (err) {
    await txCircuit.rollback();
    await txIPv4Pool.rollback();
    await txRadReply.rollback();
    throw err;
  }
}

/**
 * This function add IPv4 addresses to the pool
 * @param conf
 */
async function addIPv4RangeToPool(conf: AddIPv4Conf) {
  console.log(`Creating IPv4 pool using: ${conf.subnet}...`);
  const txIPv4Pool = await ipv4PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

  try {
    const range = new IPCIDR(conf.subnet);

    if (!range.isValid()) {
      throw new Error('CIDR is invalid');
    }

    // TODO: Remove this ignore, bad type definitions
    // @ts-ignore
    const addrs = range.toArray({ type: "addressObject" });
    for (const x in addrs) {
      const item = <any>addrs[x];
      await ipv4PoolRepository.create(new IPv4Pool({
        ip: item.addressMinusSuffix,
        isAvailable: conf.available
      }), { transaction: txIPv4Pool });
    }
    txIPv4Pool.commit();
  } catch (err) {
    console.log(err);
    txIPv4Pool.rollback();
  }
}

/**
 *
 * @param conf
 */
async function addIPv6RangeToPool(conf: AddIPv6Conf) {
  console.log(`Creating IPv6 pool using: ${conf.subnet}...`);
  const txIPv6Pool = await ipv6PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
  try {
    const range = new Address6(conf.subnet);
    const stopI = range.endAddress().bigInteger();
    let step = new BigInteger('1');
    // Shift the steper
    step = step.shiftLeft(128 - conf.toMask);

    let i = range.startAddress().bigInteger();
    while (i.compareTo(stopI) <= 0) {
      const ipv6 = Address6.fromBigInteger(i);
      await ipv6PoolRepository.create({
        prefix: `${ipv6.correctForm()}/${conf.toMask}`,
        isAvailable: conf.available
      });
      // We add step prefix
      i = i.add(step);
    }

    txIPv6Pool.commit();
  } catch (err) {
    console.log(err);
    txIPv6Pool.rollback();
  }
}

/**
 * This function removes IPv4 addresses to the pool
 * @param conf
 */
async function removeIPv4RangePool(conf: { subnet: string }) {
  console.log(`Remove IPv4 pool using: ${conf.subnet}...`);
  const txIPv4Pool = await ipv4PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

  try {
    const range = new IPCIDR(conf.subnet);

    if (!range.isValid()) {
      throw new Error('CIDR is invalid');
    }

    // TODO: Remove this ignore, bad type definitions
    // @ts-ignore
    const addrs = range.toArray({ type: "addressObject" });

    for (const x in addrs) {
      // @ts-ignore
      const ipv4 = <string>addrs[x].addressMinusSuffix;
      const item = await ipv4PoolRepository.findOne({ where: { ip: ipv4 } });
      if (!item) {
        throw new Error(`No IP found within this pool: ${ipv4}`);
      }
      if (!item.isAvailable) {
        throw new Error(`IP unavailable found within this pool: ${ipv4}`);
      }
      await ipv4PoolRepository.delete(item);
    }

    txIPv4Pool.commit();
  } catch (err) {
    console.log(err);
    txIPv4Pool.rollback();
  }
}

const options: any = {
  initialsubs: createInitialSubs,
  addipv4range: addIPv4RangeToPool,
  addipv6range: addIPv6RangeToPool,
  migrateipv4: migrateIpv4Circuits,
  removeipv4range: removeIPv4RangePool
}
// We load argv in Java Script Object
const argv = minimist(process.argv.slice(2));
const items = argv['_'];
const conf = (argv['o']) ? JSON.parse(argv['o']) : null;

async function runCLI() {
  for (let x in items) {
    const item = options[items[x]];
    if (item) {
      await item(conf);
    }
  }
}

runCLI().then((value) => {
  db.disconnect().finally(() => process.exit(0));
}).catch((err) => {
  console.log(err);
  db.disconnect().finally(() => process.exit(1));
});

