/**
 * Script used to check consistency between circuits, RADIUS tables and IP pools
 */
import 'minimist';
import * as path from 'path';
import * as fs from 'fs';
import {
  SubscriptionRepository, CircuitRepository,
  SubscriptionIdsRepository,
  IPv4PoolRepository,
  IPv6PoolRepository,
  RadcheckRepository,
  RadgroupcheckRepository,
  RadgroupreplyRepository,
  RadpostauthRepository,
  RadreplyRepository,
  RadusergroupRepository,
  UserinfoRepository,
  TransportRepository
} from '../repositories';
import { RadiusDataSource } from "../datasources/radius.datasource";
import { SimplexDataSource } from '../datasources';

const db: RadiusDataSource = new RadiusDataSource();
const simplexDb: SimplexDataSource = new SimplexDataSource();

const subscriptonIdsRepository = new SubscriptionIdsRepository(simplexDb);
const ipv4PoolRepository = new IPv4PoolRepository(simplexDb);
const ipv6PoolRepository = new IPv6PoolRepository(simplexDb);
const radCheckRepository = new RadcheckRepository(db);
const radGroupCheckRepository = new RadgroupcheckRepository(db);
const radGroupReplyRepository = new RadgroupreplyRepository(db);
const radPostAuthRepository = new RadpostauthRepository(db);
const radReplyRepository = new RadreplyRepository(db);
const radUserGroupRepository = new RadusergroupRepository(db);
const userInfoRepository = new UserinfoRepository(db);

let subscriptionRepository: SubscriptionRepository;
let circuitRepository: CircuitRepository;
let transportRepository: TransportRepository;

subscriptionRepository = new SubscriptionRepository(simplexDb, async () => circuitRepository);
circuitRepository = new CircuitRepository(simplexDb, async () => subscriptionRepository,
  async () => transportRepository);

transportRepository = new TransportRepository(simplexDb, async () => circuitRepository);

// We attach simplex configuration file
const simplexConfig: SimplexConfig = JSON.parse(
  fs.readFileSync(path.resolve('simplex-config.json')).toString()
);

import {
  SubscriptionIds,
  Circuit,
  Userinfo,
  Radcheck,
  Radusergroup,
  Radreply,
  IPv4Pool,
  IPv6Pool,
  Subscription,
} from '../models';
import minimist from 'minimist';
import { SimplexConfig } from '../application';
import { Address6 } from 'ip-address';

async function checkAvailableIPsFromCircuit(fix?: boolean) {
  // Check for IPv4Pool
  console.log('Start Check for available IPs from circuits in database...')
  const circuits = await circuitRepository.find();

  for (const x in circuits) {
    const circuit = circuits[x];

    // Check if the IPv4 is in the pool
    const ipv4 = await ipv4PoolRepository.find({ where: { ip: circuit.ipv4.valueOf() } });

    if (!ipv4.length) {
      console.log(`Inconsistency: ${circuit.username}, ${circuit.ipv4}`);
      if (fix) {
        console.log(`Fixing: ${circuit.username}, ${circuit.ipv4}`);
        await ipv4PoolRepository.create({
          ip: circuit.ipv4.valueOf(),
          isAvailable: false,
        });
      }
      // Check if it's marked to unavailable
    } else if (ipv4[0].isAvailable) {
      console.log(`Inconsistency: ${circuit.username}, ${circuit.ipv4}`);
      if (fix) {
        console.log(`Fixing: ${circuit.username}, ${circuit.ipv4}`);
        ipv4[0].isAvailable = false;
        await ipv4PoolRepository.update(ipv4[0]);
      }
    }

    // Check if the IPv6 is in the pool
    const ipv6 = await ipv6PoolRepository.find({
      where: {
        prefix: (circuit.ipv6) ? circuit.ipv6.valueOf() : ''
      }
    });
    // We must test if circuit.ipv6 exists
    if (!ipv6.length && circuit.ipv6) {
      console.log(`Inconsistency: ${circuit.username}, ${circuit.ipv6}`);
      if (fix) {
        console.log(`Fixing: ${circuit.username}, ${circuit.ipv6}`);
        await ipv6PoolRepository.create({
          prefix: circuit.ipv6.valueOf(),
          isAvailable: false,
        });
      }
    } else if (ipv6.length && ipv6[0].isAvailable) {
      console.log(`Inconsistency: ${circuit.username}, ${circuit.ipv6}`);
      if (fix) {
        console.log(`Fixing: ${circuit.username}, ${circuit.ipv6}`);
        ipv6[0].isAvailable = false;
        await ipv6PoolRepository.update(ipv6[0]);
      }
    }

  }
}

async function checkAvailableIPsFromRADIUS(fix?: boolean) {
  // Check for IPv4Pool
  console.log('Start Check for available IPs from RADIUS in database...')
  const replies = await radReplyRepository.find();

  for (const x in replies) {
    const reply = replies[x];
    if (reply.attribute === 'Framed-IP-Address') {
      // Check if the IPv4 is in the pool
      const ipv4 = await ipv4PoolRepository.find({ where: { ip: reply.value.valueOf() } });

      if (!ipv4.length) {
        console.log(`Inconsistency: ${reply.username}, ${reply.value.valueOf()}`);
        if (fix) {
          console.log(`Fixing: ${reply.username}, ${reply.value.valueOf()}`);
          await ipv4PoolRepository.create({
            ip: reply.value.valueOf(),
            isAvailable: false,
          });
        }
      }
    } else if (reply.attribute === 'Delegated-IPv6-Prefix') {
      // Check if the IPv6 is in the pool
      const ipv6 = await ipv6PoolRepository.find({
        where: {
          prefix: reply.value.valueOf()
        }
      });

      if (!ipv6.length) {
        console.log(`Inconsistency: ${reply.username}, ${reply.value.valueOf()}`);
        if (fix) {
          console.log(`Fixing: ${reply.username}, ${reply.value.valueOf()}`);
          await ipv6PoolRepository.create({
            prefix: reply.value.valueOf(),
            isAvailable: false,
          });
        }
      }
    }
  }
}

/**
 * This function is a filter to match RADIUS multiple circuit subscriptions identified by new interfaceNasId
 * @param fix
 */
async function syncSimplexWithFreeradius(fix?: boolean) {
  console.log('Start Check for sync RADIUS filter...')
  const replies = await radReplyRepository.find();

  const reg = /i(.*)c(.*)/i;
  for (const x in replies) {
    const reply = replies[x];
    if (reply.attribute === 'NAS-Port-Id') {
      // Match regex subscriptionId and circuitNum
      const match = reg.exec(reply.value.valueOf());
      if (match && match.length == 3) {
        const id = parseInt(match[1]);
        const circuitNum = parseInt(match[2]);

        // Check if id is set as unavailable in subscriptionids
        try {
          const subscriptionId = await subscriptonIdsRepository.findById(id);
          if (subscriptionId.isAvailable) {
            console.log(`Inconsistency: id ${id} is set as available and it's used`);
            if (fix) {
              console.log(`Fixing: id ${id} is set as available and it's used`);
              subscriptionId.isAvailable = false;
              await subscriptonIdsRepository.update(subscriptionId);
            }
          }
        } catch (err) {
          if (err.code && err.code === 'ENTITY_NOT_FOUND') {
            console.log(`Inconsistency: id ${err.entityId} is not found in pool`);
            if (fix && (err.entityId >= simplexConfig.subscriptionsMin
              && err.entityId <= simplexConfig.subscriptionsMax)) {
              // We should add this subscriptionId as used
              console.log(`Fixing: id ${err.entityId} is not found in pool`);
              await subscriptonIdsRepository.create(new SubscriptionIds({
                id: err.entityId,
                isAvailable: false
              }));
            }
          }
        }
        // Check subscription
        try {
          await subscriptionRepository.findById(id);
        } catch (err) {
          if (err.code && err.code === 'ENTITY_NOT_FOUND') {
            console.log(`Inconsistency: id ${err.entityId} is not found in Subscriptions table`);
            if (fix && (err.entityId >= simplexConfig.subscriptionsMin
              && err.entityId <= simplexConfig.subscriptionsMax)) {
              console.log(`Fixing: id ${err.entityId} is not found in Subscriptions table`);

              // Get userinfo
              const userinfo = await userInfoRepository.findOne({
                where:
                  { username: reply.username }
              });

              let firstname, lastname;
              firstname = (userinfo) ? (userinfo.firstname) : reply.value;
              lastname = (userinfo) ? (userinfo.lastname) : reply.value;

              await subscriptionRepository.create(new Subscription({
                id: err.entityId,
                firstname,
                lastname
              }));
            }
          }
        }
        // We get ipv4 and ipv6 for this circuit
        const circuitReplies = await radReplyRepository.find({ where: { username: reply.username } });
        // Now we should take ipv4 and ipv6
        let ipv4, ipv6;
        circuitReplies.forEach((item) => {
          if (item.attribute === 'Framed-IP-Address') ipv4 = item.value;
          else if (item.attribute === 'Delegated-IPv6-Prefix') ipv6 = item.value;
        });
        // Check circuit (create)
        try {
          const circuit = await circuitRepository.find({ where: { nasId: reply.value.valueOf() } });
          if (!circuit.length) {
            console.log(`Inconsistency: ${reply.value} is not found in Circuits table`);
            if (fix && (id >= simplexConfig.subscriptionsMin
              && id <= simplexConfig.subscriptionsMax)) {
              // We should get password
              const check = await radCheckRepository.find({ where: { username: reply.username } });

              if (!check.length) {
                throw new Error(`No password for ${reply.username}`);
              }
              console.log(`Fixing: ${reply.value} is not found in Circuits table`);
              await circuitRepository.create(new Circuit({
                circuitNum,
                ipv4,
                ipv6,
                nasId: reply.value,
                isDisabled: false,
                subscriptionId: id,
                username: reply.username,
                password: check[0].value
              }));
            }
          }
        } catch (err) {
          console.log(err);
        }
        // Check duplicate circuit (delete no c < 1)
        const circuits = await circuitRepository.find({ where: { ipv4 } });
        if (circuits.length > 1) {
          // Inconsistency multiple circuits for one ipv4
          console.log(`Inconsistency: ${ipv4} is duplicated Circuits table`);
          if (fix && (id >= simplexConfig.subscriptionsMin
            && id <= simplexConfig.subscriptionsMax)) {
            console.log(`Fixing: ${ipv4} is duplicated Circuits table`);
            for (const x in circuits) {
              if (circuits[x].circuitNum == 0) {
                await circuitRepository.delete(circuits[x]);
              }
            }
          }
        }
      }
    }
  }

  // Sync management interfaces
  const regM = /m(.*)c(.*)/i;
  for (const x in replies) {
    const reply = replies[x];
    if (reply.attribute === 'NAS-Port-Id') {
      // Match regex subscriptionId and circuitNum
      const match = regM.exec(reply.value.valueOf());
      if (match && match.length == 3) {
        const id = parseInt(match[1]);
        const circuitNum = parseInt(match[2]);
        console.log(`Inconsistency: ${reply.value} is not found in Circuits table`);

        try {
          const circuit = await circuitRepository.findOne({ where: { subscriptionId: id, circuitNum } });
          if (!circuit) {
            throw new Error(`No circuit with ${reply.value}`);
          }
          if (fix) {
            circuit.hasManagement = true;
            await circuitRepository.update(circuit);
          }
        } catch (err) {
          console.log(err);
        }
      }
    }
  }
}

async function checkVoidSubscriptions(fix?: boolean) {
  // Check void subscriptions
  console.log('Start Check void subscriptions (subs without circuits)');
  const res = await db.execute('SELECT s.id FROM subscription s \
  WHERE s.id NOT IN (SELECT subscriptionId FROM circuit)');
  if (res.length) {
    console.log('Inconsistencies:', res);
    if (fix) {
      for (const x in res) {
        console.log(`Fixing: subscription ${res[x].id}`);
        const sub = await subscriptionRepository.findById(res[x].id);
        const subPool = await subscriptonIdsRepository.findById(res[x].id);
        subPool.isAvailable = true;
        await subscriptionRepository.delete(sub);
        await subscriptonIdsRepository.update(subPool);
      }
    }
  }
}

async function checkCorrectIPv6Notation(fix?: boolean) {
  // Test ipv6 for RADIUS
  console.log('Start Check for IPv6 RADIUS filter...')
  const replies = await radReplyRepository.find();

  for (const x in replies) {
    const reply = replies[x];
    if (reply.attribute === 'Delegated-IPv6-Prefix') {
      // Load ipv6
      const ipv6 = new Address6(reply.value.valueOf());
      const correctIPv6 = `${ipv6.correctForm()}/${ipv6.subnetMask}`;
      if (reply.value.valueOf() !== correctIPv6) {
        // Incorrect form
        console.log(`Incorrect form: ${reply.value}`);
        if (fix) {
          reply.value = correctIPv6;
          await radReplyRepository.update(reply);
        }
      }
    }
  }
}

const options: any = {
  ipscircuit: checkAvailableIPsFromCircuit,
  ipsradius: checkAvailableIPsFromRADIUS,
  syncradius: syncSimplexWithFreeradius,
  voidsubs: checkVoidSubscriptions,
  ipv6notation: checkCorrectIPv6Notation
}
// We load argv in Java Script Object
const argv = minimist(process.argv.slice(2));
const items = argv['_'];
const fix = argv['f'];

async function runCLI() {
  for (let x in items) {
    const item = options[items[x]];
    if (item) {
      await item(fix);
    }
  }
}

runCLI().then((value) => {
  process.exit(0);
}).catch((err) => {
  console.log(err);
  process.exit(1);
});

