import { ExoApiApplication } from './application';

export async function migrate(args: string[]) {
  const existingSchema = args.includes('--rebuild') ? 'drop' : 'alter';
  console.log('Migrating schemas (%s existing schema)', existingSchema);

  const app = new ExoApiApplication();
  await app.boot();
  // We only add custom eXO models
  await app.migrateSchema({ existingSchema /*, models: ['SubscriptionIds', 'Circuit', 'Subscription', 'IPv4Pool', 'IPv6Pool']*/ });

  // Connectors usually keep a pool of opened connections,
  // this keeps the process running even after all work is done.
  // We need to exit explicitly.
  process.exit(0);
}

migrate(process.argv).catch(err => {
  console.error('Cannot migrate database schema', err);
  process.exit(1);
});
