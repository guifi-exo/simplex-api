import { bind, BindingScope, service, inject } from '@loopback/core';
import * as radius from 'radius';
import { SubscriptionsManagerService } from './subscriptions-manager.service';
import * as dgram from 'dgram';
import { SimplexConfig } from '../application';

@bind({ scope: BindingScope.TRANSIENT })
export class RadiusManagerService {
  constructor(@service(SubscriptionsManagerService)
  public subscriptionsManager: SubscriptionsManagerService,
    @inject('config.simplex')
    public simplexConfig: SimplexConfig, ) { }

  private async sendDisconnectUser(username: string) {
    return new Promise((resolve, reject) => {
      let closed = false;

      const packet = radius.encode({
        code: "Disconnect-Request",
        secret: this.simplexConfig.radius.daeSecret,
        attributes: [
          ['NAS-IP-Address', this.simplexConfig.radius.daeHost],
          ['User-Name', username],
        ]
      });

      const client = dgram.createSocket('udp4');
      client.send(packet, this.simplexConfig.radius.daePort,
        this.simplexConfig.radius.daeHost, (err) => {
          if (err) {
            client.close();
            closed = true;
            reject(err);
          }
        });

      client.on('message', (msg, info) => {
        const radpacket = radius.decode({
          secret: this.simplexConfig.radius.daeSecret,
          packet: msg
        });
        // Test packet
        if (radpacket.code === 'Disconnect-ACK') {
          client.close();
          closed = true;
          resolve({
            msg: 'ok'
          });
        } else {
          reject({ msg: radpacket });
        }
      });
      setTimeout((arg: any) => {
        if (!closed) {
          client.close();
          reject(arg);
        }
      }, this.simplexConfig.radius.timeout, {
        msg: 'Timeout exceeded in RADIUS response'
      });
    });
  }

  public async disconnectUser(id: number) {
    const circuit = await this.subscriptionsManager.getCircuit(id);

    return this.sendDisconnectUser(
      `${circuit.username.valueOf()}@${this.simplexConfig.radius.realm}`
    );
  }

}
