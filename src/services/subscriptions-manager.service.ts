import { bind, BindingScope, inject } from '@loopback/core';
import {
  RadreplyRepository, SubscriptionIdsRepository, UserinfoRepository,
  RadcheckRepository, RadusergroupRepository, SubscriptionRepository, CircuitRepository,
  TransportRepository
} from '../repositories';
import { Userinfo, Radcheck, Radusergroup, Radreply, Subscription, Circuit } from '../models';
import { IsolationLevel, Transaction } from '@loopback/repository';
import { BatchUser } from '../controllers/radius.controller';
import generatePassword from 'password-generator';
import { IPv4PoolRepository } from '../repositories/i-pv-4-pool.repository';
import { IPv6PoolRepository } from '../repositories/i-pv-6-pool.repository';
import { SimplexConfig } from '../application';
import { HttpErrors } from '@loopback/rest';

export interface SubscriptionTransactions {
  radReplyTx?: Transaction;
  subscriptionIdsTx?: Transaction;
  userinfoTx?: Transaction;
  radcheckTx?: Transaction;
  radusergroupTx?: Transaction;
  subscriptionTx?: Transaction;
  circuitTx?: Transaction;
  ipv4PoolTx?: Transaction;
  ipv6PoolTx?: Transaction;
  transportTx?: Transaction;
}

export interface SubscriptionData {
  firstname: string;
  lastname?: string;
}

export interface CircuitData {
  username?: string;
  password?: string;
  subscriptionId: number;
  ipv4?: string;
  ipv6?: string;
  hasManagement: boolean;
}

export interface CircuitFilterData {
  ipv4?: string;
  nasId?: string;
}

export interface AddCircuitData {
  hasManagement: boolean;
  ipv4?: string;
  ipv6?: string;
  username?: string;
  password?: string;
}

@bind({ scope: BindingScope.TRANSIENT })
export class SubscriptionsManagerService {
  constructor(
    @inject('repositories.RadreplyRepository')
    public radReplyRepository: RadreplyRepository,
    @inject('repositories.SubscriptionIdsRepository')
    public subscriptionIdsRepository: SubscriptionIdsRepository,
    @inject('repositories.UserinfoRepository')
    public userinfoRepository: UserinfoRepository,
    @inject('repositories.RadcheckRepository')
    public radcheckRepository: RadcheckRepository,
    @inject('repositories.RadusergroupRepository')
    public radusergroupRepository: RadusergroupRepository,
    @inject('repositories.SubscriptionRepository')
    public subscriptionRepository: SubscriptionRepository,
    @inject('repositories.CircuitRepository')
    public circuitRepository: CircuitRepository,
    @inject('repositories.IPv4PoolRepository')
    public ipv4PoolRepository: IPv4PoolRepository,
    @inject('repositories.IPv6PoolRepository')
    public ipv6PoolRepository: IPv6PoolRepository,
    @inject('repositories.TransportRepository')
    public transportRepository: TransportRepository,
    @inject('config.simplex')
    public simplexConfig: SimplexConfig, ) { }

  // Private methods
  /** type 0: Static (i) username
   *  type 1: Management (m) username
  */
  private buildUsername(type: number, subscriptionId: number, circuitNum: number) {
    let res = '';
    switch (type) {
      case 0:
        res = `i${subscriptionId.toString().padStart(4, '0')}c${circuitNum}`;
        break;
      case 1:
        res = `m${subscriptionId.toString().padStart(4, '0')}c${circuitNum}`;
        break;
    }
    return res;
  }

  private async addSingleSubscription(data: any, transactions: SubscriptionTransactions) {
    // Get id subscription
    const id = await this.subscriptionIdsRepository.find(
      { where: { isAvailable: true }, limit: 1, order: ['id ASC'] }, { transaction: transactions.subscriptionIdsTx });

    // We create subscription record
    const subscription = new Subscription();
    subscription.firstname = data.firstname;
    subscription.lastname = data.lastname;
    subscription.id = id[0].id;

    const res = await this.subscriptionRepository.create(subscription, { transaction: transactions.subscriptionTx });

    // Set id as used
    const newId = id[0];
    newId.isAvailable = false;
    await this.subscriptionIdsRepository.update(newId, { transaction: transactions.subscriptionIdsTx });

    return res;
  }

  private async removeSingleSubscription(id: number, transactions: SubscriptionTransactions) {
    // We find subscription entry
    const subscription = await this.subscriptionRepository.findById(id);

    // We find and set subscriptionId to available
    const subscriptionId = await this.subscriptionIdsRepository.findById(id);
    subscriptionId.isAvailable = true;
    await this.subscriptionIdsRepository.update(subscriptionId, { transaction: transactions.subscriptionIdsTx });

    // We remove subscription
    await this.subscriptionRepository.delete(subscription);
  }

  private async addSingleCircuit(data: CircuitData, transactions: SubscriptionTransactions) {
    const assignedCircuits = await this.circuitRepository.find(
      { where: { subscriptionId: data.subscriptionId }, order: ['circuitNum ASC'] }, { transaction: transactions.circuitTx });

    let circuitNum = 0;
    for (const x in assignedCircuits) {
      if (assignedCircuits[x].circuitNum != circuitNum)
        break;
      ++circuitNum;
    }

    let ipv4 = data.ipv4;
    let ipv4result;
    // ipv4 is specified by client, mark it as used in the ipv4 pool
    if (ipv4) {
      ipv4result = await this.ipv4PoolRepository.find(
        { where: { ip: ipv4 }}, { transaction: transactions.ipv4PoolTx });

      if (!Array.isArray(ipv4result) || !ipv4result.length) {
        throw new HttpErrors.FailedDependency(`IPv4 address ${ipv4} not found in pool repository`);
      }
    // ipv4 is specified by client, mark it as used in the ipv4 pool
    } else {
      ipv4result = await this.ipv4PoolRepository.find(
        { where: { isAvailable: true }, limit: 1, order: ['ip ASC'] }, { transaction: transactions.ipv4PoolTx });

      if (!Array.isArray(ipv4result) || !ipv4result.length) {
        throw new HttpErrors.FailedDependency('No IPv4 address available');
      }
    }

    ipv4 = ipv4result[0].ip;
    // Set ip as used
    const newIpv4 = ipv4result[0];
    newIpv4.isAvailable = false;
    await this.ipv4PoolRepository.update(newIpv4, { transaction: transactions.ipv4PoolTx });

    let ipv6 = data.ipv6;
    let ipv6result;
    // ipv6 is specified by client, mark it as used in the ipv6 pool
    if (ipv6) {
      ipv6result = await this.ipv6PoolRepository.find(
        { where: { prefix: ipv6 }}, { transaction: transactions.ipv6PoolTx });

      if (!Array.isArray(ipv6result) || !ipv6result.length) {
        throw new HttpErrors.FailedDependency(`IPv6 prefix ${ipv6} not found in pool repository`);
      }
    // ipv6 is not specified, then get the next one available in the ipv6 pool
    } else {
      ipv6result = await this.ipv6PoolRepository.find(
        { where: { isAvailable: true }, limit: 1, order: ['prefix ASC'] }, { transaction: transactions.ipv6PoolTx });

      if (!Array.isArray(ipv6result) || !ipv6result.length) {
        throw new HttpErrors.FailedDependency('No IPv6 prefix available');
      }
    }

    ipv6 = ipv6result[0].prefix;
    // Set ipv6 as used
    const newIpv6 = ipv6result[0];
    newIpv6.isAvailable = false;
    await this.ipv6PoolRepository.update(newIpv6, { transaction: transactions.ipv6PoolTx });

    const circuit = new Circuit()
    circuit.circuitNum = circuitNum;
    circuit.nasId = this.buildUsername(0, data.subscriptionId, circuitNum);
    circuit.username = (data.username) ? data.username : circuit.nasId;
    circuit.subscriptionId = data.subscriptionId;
    circuit.password = (data.password) ? data.password : generatePassword(10, false);
    circuit.ipv4 = ipv4;
    circuit.ipv6 = ipv6;
    circuit.hasManagement = data.hasManagement;

    return await this.circuitRepository.create(circuit, { transaction: transactions.circuitTx });
  }

  private async removeSingleCircuit(circuit: Circuit, transactions: SubscriptionTransactions) {
    // We set as available circuit IPs
    const ipv4 = await this.ipv4PoolRepository.findById(circuit.ipv4.toString());
    ipv4.isAvailable = true;
    await this.ipv4PoolRepository.update(ipv4, { transaction: transactions.ipv4PoolTx });

    const ipv6 = await this.ipv6PoolRepository.findById(circuit.ipv6.toString());
    ipv6.isAvailable = true;
    await this.ipv6PoolRepository.update(ipv6, { transaction: transactions.ipv6PoolTx });

    // We remove circuit
    await this.circuitRepository.delete(circuit);

  }

  private async enableSingleCircuit(id: number, transactions: SubscriptionTransactions) {
    const circuit = await this.circuitRepository.findById(id);
    circuit.isDisabled = false;
    await this.circuitRepository.update(circuit, { transaction: transactions.circuitTx });
    await this.unsetDisabledRADIUS(circuit.username.valueOf(), transactions);
  }

  private async disableSingleCircuit(id: number, transactions: SubscriptionTransactions) {
    const circuit = await this.circuitRepository.findById(id);
    if (!circuit.isDisabled) {
      circuit.isDisabled = true;
      await this.circuitRepository.update(circuit, { transaction: transactions.circuitTx });
      await this.setDisabledRADIUS(circuit.username.valueOf(), transactions);
    }
    // TODO: else throw error "It's already disabled!"
  }

  /* TODO It's not ready
  private async updateSingleCircuit(id: number, data: Circuit, transactions: SubscriptionTransactions) {
    let circuit = await this.circuitRepository.findById(id);
    if (circuit) {
      if (circuit.isDisabled && !data.isDisabled) {
        // We should enable circuit
        await this.unsetDisabledRADIUS(circuit.username.valueOf(), transactions);
      } else if (!circuit.isDisabled && data.isDisabled) {
        // We should disable circuit
        await this.setDisabledRADIUS(circuit.username.valueOf(), transactions);
      }

      circuit.isDisabled = (data.isDisabled)

      await this.circuitRepository.update(circuit, { transaction: transactions.circuitTx });

    } else {
      // TODO: Improve error management
      throw new Error("No exist this record");
    }
  }*/


  private async writeToRADIUS(circuit: Circuit, transactions: SubscriptionTransactions, isManagement: boolean) {
    // 2: Add user to Userinfo repo
    const userInfo = new Userinfo();
    userInfo.username = circuit.username;
    userInfo.creationby = 'administrator'; // TODO: Remove hardcoded
    userInfo.creationdate = new Date();
    userInfo.updatedate = new Date();

    // Wait for user creation
    await this.userinfoRepository.create(userInfo, { transaction: transactions.userinfoTx });

    // 3: Add user password
    const userRadCheck = new Radcheck();
    userRadCheck.username = circuit.username;
    userRadCheck.op = ':=';
    userRadCheck.attribute = 'Cleartext-Password'
    userRadCheck.value = circuit.password;

    await this.radcheckRepository.create(userRadCheck, { transaction: transactions.radcheckTx });

    // 4: Add user to group
    const userRadGroup = new Radusergroup();
    userRadGroup.username = circuit.username;
    userRadGroup.groupname = (isManagement) ? this.simplexConfig.managementRadGroup
      : this.simplexConfig.staticRadGroup;
    userRadGroup.priority = 1;

    await this.radusergroupRepository.create(userRadGroup, { transaction: transactions.radusergroupTx });

    // 5: Add IPs to subscription & nas-id
    if (!isManagement) {
      const userReplyIpv4 = new Radreply();
      userReplyIpv4.username = circuit.username;
      userReplyIpv4.op = ':=';
      userReplyIpv4.attribute = 'Framed-IP-Address';
      userReplyIpv4.value = circuit.ipv4;

      await this.radReplyRepository.create(userReplyIpv4, { transaction: transactions.radReplyTx });

      if (circuit.ipv6) {
        const userReplyIpv6 = new Radreply();
        userReplyIpv6.username = circuit.username;
        userReplyIpv6.op = ':=';
        userReplyIpv6.attribute = 'Delegated-IPv6-Prefix';
        userReplyIpv6.value = circuit.ipv6;

        await this.radReplyRepository.create(userReplyIpv6, { transaction: transactions.radReplyTx });
      }
    }

    // 6. Add nas-port-id
    const userReplyNas = new Radreply();
    userReplyNas.username = circuit.username;
    userReplyNas.op = ':=';
    userReplyNas.attribute = 'NAS-Port-Id';
    userReplyNas.value = circuit.nasId;

    await this.radReplyRepository.create(userReplyNas, { transaction: transactions.radReplyTx });
  }

  private async removeFromRADIUS(username: string, transactions: SubscriptionTransactions) {
    // Delete resources
    await this.radReplyRepository.deleteAll({ username }, { transaction: transactions.radReplyTx });

    // Delete user group
    await this.radusergroupRepository.deleteAll({ username }, { transaction: transactions.radusergroupTx });

    // Delete user password
    await this.radcheckRepository.deleteAll({ username }, { transaction: transactions.radcheckTx });

    // Delete user info entity
    await this.userinfoRepository.deleteAll({ username }, { transaction: transactions.userinfoTx });
  }

  private async setDisabledRADIUS(username: string, transactions: SubscriptionTransactions) {
    // 4: Add user to group
    const userRadGroup = new Radusergroup();
    userRadGroup.username = username;
    userRadGroup.groupname = this.simplexConfig.disableRadGroup;
    // We set this group to maximum priority (0)
    userRadGroup.priority = 0;

    await this.radusergroupRepository.create(userRadGroup, { transaction: transactions.radusergroupTx });
  }

  private async unsetDisabledRADIUS(username: string, transactions: SubscriptionTransactions) {
    // Delete user group
    await this.radusergroupRepository.deleteAll({ username, groupname: this.simplexConfig.disableRadGroup },
      { transaction: transactions.radusergroupTx });
  }

  // Public methods
  public async addSubscriptionsBatch(data: Array<BatchUser>) {
    // We prepare transactions
    const tx: SubscriptionTransactions = {};
    tx.radReplyTx = await this.radReplyRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.subscriptionIdsTx = await this.subscriptionIdsRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.userinfoTx = await this.userinfoRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radcheckTx = await this.radcheckRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radusergroupTx = await this.radusergroupRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.subscriptionTx = await this.subscriptionRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.circuitTx = await this.circuitRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

    try {
      for (const x in data) {
        const subData: SubscriptionData = {
          firstname: data[x].name,
        }

        const subscription = await this.addSingleSubscription(subData, tx);

        const circuitData: CircuitData = {
          subscriptionId: subscription.id.valueOf(),
          username: data[x].name,
          password: data[x].password,
          ipv4: data[x]['remote-address'],
          hasManagement: false
        }

        const circuit = await this.addSingleCircuit(circuitData, tx);
        await this.writeToRADIUS(circuit, tx, false);
      }
      // We commit changes
      await tx.radReplyTx.commit();
      await tx.subscriptionIdsTx.commit();
      await tx.userinfoTx.commit();
      await tx.radcheckTx.commit();
      await tx.radusergroupTx.commit();
      await tx.subscriptionTx.commit();
      await tx.circuitTx.commit();
      return { msg: 'ok' }
    } catch (err) {
      await tx.radReplyTx.rollback();
      await tx.subscriptionIdsTx.rollback();
      await tx.userinfoTx.rollback();
      await tx.radcheckTx.rollback();
      await tx.radusergroupTx.rollback();
      await tx.subscriptionTx.rollback();
      await tx.circuitTx.rollback();
      throw err;
    }
  }

  public async getSubscriptions() {
    const conf = this.simplexConfig;
    try {
      return this.subscriptionRepository.find();
    } catch (err) {
      throw err;
    }
  }

  public async getSubscription(id: number) {
    try {
      return this.subscriptionRepository.findById(id, { include: [{ relation: 'circuits' }] });
    } catch (err) {
      throw err;
    }
  }

  public async getCircuit(id: number) {
    try {
      return this.circuitRepository.findById(id, { include: [{ relation: 'subscription' }, { relation: 'transport' }] });
    } catch (err) {
      throw err;
    }
  }

  public async getCircuits(data: CircuitFilterData) {
    try {
      return this.circuitRepository.find({ include: [{ relation: 'subscription' }, { relation: 'transport' }], where: { ipv4: data.ipv4, nasId: data.nasId } });
    } catch (err) {
      throw err;
    }
  }

  public async addSubscription(data: SubscriptionData) {
    const tx: SubscriptionTransactions = {};
    tx.subscriptionIdsTx = await this.subscriptionIdsRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.subscriptionTx = await this.subscriptionRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

    try {
      const subscription = await this.addSingleSubscription(data, tx);
      await tx.subscriptionTx.commit();
      await tx.subscriptionIdsTx.commit();

      return subscription;
    } catch (err) {
      await tx.subscriptionTx.rollback();
      await tx.subscriptionIdsTx.rollback();

      throw err;
    }

  }

  public async removeSubscription(id: number) {
    const tx: SubscriptionTransactions = {};
    tx.subscriptionIdsTx = await this.subscriptionIdsRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.subscriptionTx = await this.subscriptionRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

    try {
      const subscription = await this.removeSingleSubscription(id, tx);
      await tx.subscriptionTx.commit();
      await tx.subscriptionIdsTx.commit();

      return { msg: 'ok' };
    } catch (err) {
      await tx.subscriptionTx.rollback();
      await tx.subscriptionIdsTx.rollback();

      throw err;
    }
  }

  public async addSubscriptionCircuit(id: number, data: AddCircuitData) {
    const tx: SubscriptionTransactions = {};
    tx.circuitTx = await this.circuitRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.ipv4PoolTx = await this.ipv4PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.ipv6PoolTx = await this.ipv6PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radReplyTx = await this.radReplyRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.userinfoTx = await this.userinfoRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radcheckTx = await this.radcheckRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radusergroupTx = await this.radusergroupRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

    try {
      const circuit = await this.addSingleCircuit({
        ipv4: data.ipv4,
        ipv6: data.ipv6,
        username: data.username,
        password: data.password,
        subscriptionId: id,
        hasManagement: data.hasManagement
      }, tx);

      const res = { ...circuit };

      await this.writeToRADIUS(circuit, tx, false);

      if (circuit.hasManagement) {
        circuit.username = this.buildUsername(1, circuit.subscriptionId, circuit.circuitNum.valueOf());
        circuit.nasId = circuit.username;
        await this.writeToRADIUS(circuit, tx, true);
      }

      await tx.circuitTx.commit();
      await tx.ipv4PoolTx.commit();
      await tx.ipv6PoolTx.commit();
      await tx.userinfoTx.commit();
      await tx.radcheckTx.commit();
      await tx.radReplyTx.commit();
      await tx.radusergroupTx.commit();

      return res;
    } catch (err) {
      await tx.circuitTx.rollback();
      await tx.ipv4PoolTx.rollback();
      await tx.ipv6PoolTx.rollback();
      await tx.userinfoTx.rollback();
      await tx.radcheckTx.rollback();
      await tx.radReplyTx.rollback();
      await tx.radusergroupTx.rollback();

      throw err;
    }
  }

  public async removeSubscriptionCircuit(id: number) {
    const tx: SubscriptionTransactions = {};
    tx.circuitTx = await this.circuitRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.ipv4PoolTx = await this.ipv4PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.ipv6PoolTx = await this.ipv6PoolRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radReplyTx = await this.radReplyRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.userinfoTx = await this.userinfoRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radcheckTx = await this.radcheckRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.radusergroupTx = await this.radusergroupRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

    try {
      const circuit = await this.circuitRepository.findById(id);

      await this.removeFromRADIUS(circuit.username.toString(), tx);
      if (circuit.hasManagement) {
        const username = this.buildUsername(1, circuit.subscriptionId, circuit.circuitNum.valueOf());
        this.removeFromRADIUS(username, tx);
      }

      await this.removeSingleCircuit(circuit, tx);

      await tx.circuitTx.commit();
      await tx.ipv4PoolTx.commit();
      await tx.ipv6PoolTx.commit();
      await tx.userinfoTx.commit();
      await tx.radcheckTx.commit();
      await tx.radReplyTx.commit();
      await tx.radusergroupTx.commit();

      return { msg: 'ok' };
    } catch (err) {
      await tx.circuitTx.rollback();
      await tx.ipv4PoolTx.rollback();
      await tx.ipv6PoolTx.rollback();
      await tx.userinfoTx.rollback();
      await tx.radcheckTx.rollback();
      await tx.radReplyTx.rollback();
      await tx.radusergroupTx.rollback();

      throw err;
    }
  }

  public async disableSubscriptionCircuit(id: number) {
    const tx: SubscriptionTransactions = {};

    tx.radusergroupTx = await this.radusergroupRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.circuitTx = await this.circuitRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

    try {
      await this.disableSingleCircuit(id, tx);

      await tx.circuitTx.commit();
      await tx.radusergroupTx.commit();

      return { msg: 'ok' };
    } catch (err) {
      await tx.circuitTx.rollback();
      await tx.radusergroupTx.rollback();

      throw err;
    }
  }

  public async enableSubscriptionCircuit(id: number) {
    const tx: SubscriptionTransactions = {};

    tx.radusergroupTx = await this.radusergroupRepository.beginTransaction(IsolationLevel.READ_COMMITTED);
    tx.circuitTx = await this.circuitRepository.beginTransaction(IsolationLevel.READ_COMMITTED);

    try {
      await this.enableSingleCircuit(id, tx);

      await tx.circuitTx.commit();
      await tx.radusergroupTx.commit();

      return { msg: 'ok' };
    } catch (err) {
      await tx.circuitTx.rollback();
      await tx.radusergroupTx.rollback();

      throw err;
    }
  }

  public async attachCircuitTransport(id: number, transportId: number) {
    try {
      const transport = await this.transportRepository.findById(transportId);
      const circuit = await this.circuitRepository.findById(id);
      circuit.transportId = transport.id;

      await this.circuitRepository.update(circuit);

      return { msg: 'ok' };
    } catch (err) {

      throw err;
    }
  }

  public async detachCircuitTransport(id: number) {
    try {
      const circuit = await this.circuitRepository.findById(id);
      circuit.transportId = undefined;
      await this.circuitRepository.update(circuit);
      return { msg: 'ok' };
    } catch (err) {
      throw err;
    }
  }
}
