import { post, get, param, requestBody, del, patch, getJsonSchema } from "@loopback/rest";
// Uncomment these imports to begin using these cool features!

import { inject } from '@loopback/context';
import { RadreplyRepository, SubscriptionIdsRepository, UserinfoRepository, RadcheckRepository, RadusergroupRepository } from "../repositories";
import { SubscriptionIds, Userinfo, Radcheck, Radusergroup, Radreply, Subscription, Circuit } from "../models";
import { IsolationLevel } from "@loopback/repository";
import { service } from "@loopback/core";
import { SubscriptionsManagerService, AddCircuitData, SubscriptionData, RadiusManagerService } from "../services";
import { throws } from "assert";

// TODO move to declarations class or similar
export interface BatchUser {
  name: string;
  password: string;
  ['remote-address']: string;
}

export interface BatchBody {
  data: Array<BatchUser>;
}


// Subscription Post Object specification
const specSubscriptionPost = {
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          firstname: {
            type: 'string'
          },
          lastname: {
            type: 'string'
          }
        }
      }
    }
  }
};

// Circuit Post Object specification
const specCircuitPost = {
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          hasManagement: {
            type: 'boolean'
          },
          ipv4: {
            type: 'string',
            nullable: true
          },
          ipv6: {
            type: 'string',
            nullable: true
          },
          username: {
            type: 'string',
            nullable: true
          },
          password: {
            type: 'string',
            nullable: true
          }
        }
      }
    }
  }
};

// Circuit object specification
const specCircuit = {
  type: 'object',
  properties: {
    id: {
      type: 'number'
    },
    circuitNum: {
      type: 'number'
    },
    username: {
      type: 'string'
    },
    password: {
      type: 'string'
    },
    ipv4: {
      type: 'string'
    },
    ipv6: {
      type: 'string'
    },
    nasId: {
      type: 'string'
    },
    hasManagement: {
      type: 'boolean'
    },
    subscriptionId: {
      type: 'number'
    }
  }
}

const specSubscriptionCircuit = {
  type: 'object',
  properties: {
    id: {
      type: 'number'
    },
    firstname: {
      type: 'string'
    },
    lastname: {
      type: 'string'
    },
    circuit: {
      type: 'array',
      items: specCircuit
    }
  }
};

const specOkMsg = {
  type: 'object',
  properties: {
    msg: {
      type: 'number',
      enum: ['ok']
    },
  }
};

const baseControllerPath = '/radius';

export class RadiusController {
  constructor(
    @service(SubscriptionsManagerService)
    public subscriptionsManager: SubscriptionsManagerService,
    @service(RadiusManagerService)
    public radiusManager: RadiusManagerService,
    @inject('repositories.SubscriptionIdsRepository')
    public subscriptionIdsRepository: SubscriptionIdsRepository, ) { }

  @post(baseControllerPath + '/batch')
  async subscriptionAddBatch(@requestBody() body: BatchBody) {
    return this.subscriptionsManager.addSubscriptionsBatch(body.data);
  }


  @get(baseControllerPath + '/subscription/', {
    summary: 'Get subscription records',
    description: 'Get all the subscription records stored in the database.',
    responses: {
      '200': {
        description: 'List with all the subscriptions stored in the database',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: {
                'x-ts-type': Subscription,
              }
            },
          },
        },
      },
    },
  })
  async getSubscriptions() {
    return this.subscriptionsManager.getSubscriptions();
  }

  @post(baseControllerPath + '/subscription/', {
    summary: 'Creates one subscription',
    description: 'Creates one subscription in the database',
    responses: {
      '200': {
        description: 'Returns a new subscription record.',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': Subscription,
            },
          },
        },
      },
    },
  })
  async addSubscription(
    @requestBody(specSubscriptionPost) body: SubscriptionData,
  ) {
    return this.subscriptionsManager.addSubscription(body);
  }

  @get(baseControllerPath + '/subscription/{subscription}', {
    summary: 'Get one subscription',
    description: 'Get one subscription and if it has circuits also get circuit records',
    responses: {
      '200': {
        description: 'Subscription data from given id with its circuits',
        content: {
          'application/json': {
            schema: specSubscriptionCircuit,
          },
        },
      },
    },
  })
  async getSubscription(
    @param.path.number('subscription') id: number,
  ) {
    return this.subscriptionsManager.getSubscription(id);
  }

  @del(baseControllerPath + '/subscription/{subscription}', {
    summary: 'Delete one subscription',
    description: 'Delete one subscription from the database',
    responses: {
      '200': {
        description: 'Ok message',
        content: {
          'application/json': {
            schema: specOkMsg,
          },
        },
      },
    },
  })
  async removeSubscription(
    @param.path.number('subscription') id: number,
  ) {
    return this.subscriptionsManager.removeSubscription(id);
  }

  @post(baseControllerPath + '/subscription/{subscription}/circuit', {
    summary: 'Creates one circuit',
    description: 'Creates one circuit and assign it to given subscriptor',
    responses: {
      '200': {
        description: 'Returns a new circuit record.',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': Circuit,
            },
          },
        },
      },
    },
  })
  async addSubscriptionCircuit(
    @param.path.number('subscription') id: number,
    @requestBody(specCircuitPost) body: AddCircuitData,
  ) {
    return this.subscriptionsManager.addSubscriptionCircuit(id, body);
  }

  @del(baseControllerPath + '/subscription/{subscription}/circuit/{circuit}', {
    summary: 'Delete one circuit',
    description: 'Delete one circuit from the database',
    responses: {
      '200': {
        description: 'Ok message',
        content: {
          'application/json': {
            schema: specOkMsg,
          },
        },
      },
    },
  })
  async removeSubscriptionCircuit(
    @param.path.number('subscription') subscription: number,
    @param.path.number('circuit') circuit: number,
  ) {
    return this.subscriptionsManager.removeSubscriptionCircuit(circuit);
  }

  @get(baseControllerPath + '/subscription/circuit/{circuit}', {
    summary: 'Get one circuit',
    description: 'Get one circuit from given id',
    responses: {
      '200': {
        description: 'Returns the circuit record',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': Circuit,
            },
          },
        },
      },
    },
  })
  async getCircuit(
    @param.path.number('circuit') id: number,
  ) {
    return this.subscriptionsManager.getCircuit(id);
  }

  @get(baseControllerPath + '/subscription/circuit', {
    summary: 'Get one circuit',
    description: 'Get one circuit from given ipv4 or nasId',
    responses: {
      '200': {
        description: 'Returns the circuit record',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': Circuit,
            },
          },
        },
      },
    },
  })
  async getCircuits(
    @param.query.string('nasId') nasId: string,
    @param.query.string('ipv4') ipv4: string,
  ) {
    return this.subscriptionsManager.getCircuits({
      ipv4,
      nasId
    });
  }

  @patch(baseControllerPath + '/subscription/{subscription}/circuit/{circuit}/disable', {
    summary: 'Disable one circuit',
    description: 'Disable one circuit from the database and RADIUS schema',
    responses: {
      '200': {
        description: 'Ok message',
        content: {
          'application/json': {
            schema: specOkMsg,
          },
        },
      },
    },
  })
  async disableCircuit(
    @param.path.number('subscription') subscription: number,
    @param.path.number('circuit') circuit: number,
  ) {
    return this.subscriptionsManager.disableSubscriptionCircuit(circuit);
  }

  @patch(baseControllerPath + '/subscription/{subscription}/circuit/{circuit}/enable', {
    summary: 'Enable one circuit',
    description: 'Enable one circuit from the database and RADIUS schema',
    responses: {
      '200': {
        description: 'Ok message',
        content: {
          'application/json': {
            schema: specOkMsg,
          },
        },
      },
    },
  })
  async enableCircuit(
    @param.path.number('subscription') subscription: number,
    @param.path.number('circuit') circuit: number,
  ) {
    return this.subscriptionsManager.enableSubscriptionCircuit(circuit);
  }

  @patch(baseControllerPath + '/subscription/{subscription}/circuit/{circuit}/disconnect', {
    summary: 'Disconnect one circuit',
    description: 'Disconnect one circuit from the database and RADIUS schema',
    responses: {
      '200': {
        description: 'Ok message',
        content: {
          'application/json': {
            schema: specOkMsg,
          },
        },
      },
    },
  })
  async disconnectCircuit(
    @param.path.number('subscription') subscription: number,
    @param.path.number('circuit') circuit: number,
  ) {
    return this.radiusManager.disconnectUser(circuit);
  }

  @patch(baseControllerPath + '/subscription/{subscription}/circuit/{circuit}/transport/{transport}', {
    summary: 'Attach transport to a circuit',
    description: 'Attach one transport from the database to the given circuit',
    responses: {
      '200': {
        description: 'Ok message',
        content: {
          'application/json': {
            schema: specOkMsg,
          },
        },
      },
    },
  })
  async attachTransport(
    @param.path.number('subscription') subscription: number,
    @param.path.number('circuit') circuit: number,
    @param.path.number('transport') transport: number,
  ) {
    return this.subscriptionsManager.attachCircuitTransport(circuit, transport);
  }

  @del(baseControllerPath + '/subscription/{subscription}/circuit/{circuit}/transport', {
    summary: 'Detach transport from circuit',
    description: 'Detach transport from the given circuit',
    responses: {
      '200': {
        description: 'Ok message',
        content: {
          'application/json': {
            schema: specOkMsg,
          },
        },
      },
    },
  })
  async detachTransport(
    @param.path.number('subscription') subscription: number,
    @param.path.number('circuit') circuit: number,
  ) {
    return this.subscriptionsManager.detachCircuitTransport(circuit);
  }
}
