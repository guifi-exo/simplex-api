import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Carrier} from '../models';
import {CarrierRepository} from '../repositories';

export class CarrierController {
  constructor(
    @repository(CarrierRepository)
    public carrierRepository : CarrierRepository,
  ) {}

  @post('/carrier', {
    responses: {
      '200': {
        description: 'Carrier model instance',
        content: {'application/json': {schema: getModelSchemaRef(Carrier)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Carrier, {
            title: 'NewCarrier',
            exclude: ['id'],
          }),
        },
      },
    })
    carrier: Omit<Carrier, 'id'>,
  ): Promise<Carrier> {
    return this.carrierRepository.create(carrier);
  }

  @get('/carrier/count', {
    responses: {
      '200': {
        description: 'Carrier model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Carrier) where?: Where<Carrier>,
  ): Promise<Count> {
    return this.carrierRepository.count(where);
  }

  @get('/carrier', {
    responses: {
      '200': {
        description: 'Array of Carrier model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Carrier, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Carrier) filter?: Filter<Carrier>,
  ): Promise<Carrier[]> {
    return this.carrierRepository.find(filter);
  }

  @patch('/carrier', {
    responses: {
      '200': {
        description: 'Carrier PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Carrier, {partial: true}),
        },
      },
    })
    carrier: Carrier,
    @param.where(Carrier) where?: Where<Carrier>,
  ): Promise<Count> {
    return this.carrierRepository.updateAll(carrier, where);
  }

  @get('/carrier/{id}', {
    responses: {
      '200': {
        description: 'Carrier model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Carrier, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Carrier, {exclude: 'where'}) filter?: FilterExcludingWhere<Carrier>
  ): Promise<Carrier> {
    return this.carrierRepository.findById(id, filter);
  }

  @patch('/carrier/{id}', {
    responses: {
      '204': {
        description: 'Carrier PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Carrier, {partial: true}),
        },
      },
    })
    carrier: Carrier,
  ): Promise<void> {
    await this.carrierRepository.updateById(id, carrier);
  }

  @put('/carrier/{id}', {
    responses: {
      '204': {
        description: 'Carrier PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() carrier: Carrier,
  ): Promise<void> {
    await this.carrierRepository.replaceById(id, carrier);
  }

  @del('/carrier/{id}', {
    responses: {
      '204': {
        description: 'Carrier DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.carrierRepository.deleteById(id);
  }
}
